/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Macedo
 */
public class Mquarto {
   private int idQuarto;
   private String numQuarto;
   private String descQuarto;
   private String statusQuarto;
   private String tipoQuarto;
   private String caracQuarto;

    public Mquarto() {
    }

    public Mquarto(int idQuarto, String numQuarto, String descQuarto, String statusQuarto, String tipoQuarto, String caracQuarto) {
        this.idQuarto = idQuarto;
        this.numQuarto = numQuarto;
        this.descQuarto = descQuarto;
        this.statusQuarto = statusQuarto;
        this.tipoQuarto = tipoQuarto;
        this.caracQuarto = caracQuarto;
    }

    public int getIdQuarto() {
        return idQuarto;
    }

    public void setIdQuarto(int idQuarto) {
        this.idQuarto = idQuarto;
    }

    public String getNumQuarto() {
        return numQuarto;
    }

    public void setNumQuarto(String numQuarto) {
        this.numQuarto = numQuarto;
    }

    public String getDescQuarto() {
        return descQuarto;
    }

    public void setDescQuarto(String descQuarto) {
        this.descQuarto = descQuarto;
    }

    public String getStatusQuarto() {
        return statusQuarto;
    }

    public void setStatusQuarto(String statusQuarto) {
        this.statusQuarto = statusQuarto;
    }

    public String getTipoQuarto() {
        return tipoQuarto;
    }

    public void setTipoQuarto(String tipoQuarto) {
        this.tipoQuarto = tipoQuarto;
    }

    public String getCaracQuarto() {
        return caracQuarto;
    }

    public void setCaracQuarto(String caracQuarto) {
        this.caracQuarto = caracQuarto;
    }
    
   
   
    
}
