/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class utilizacao {
    private int cod;
    private Date dtinicio;
    private Date dtfim;
    private String solicitacao;
    
    public utilizacao(){
        
    }

    public utilizacao(int cod, Date dtinicio, Date dtfim, String solicitacao) {
        this.cod = cod;
        this.dtinicio = dtinicio;
        this.dtfim = dtfim;
        this.solicitacao = solicitacao;
    }

    public String getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(String solicitacao) {
        this.solicitacao = solicitacao;
    }

   

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    

    public Date getDtinicio() {
        return dtinicio;
    }

    public void setDtinicio(Date dtinicio) {
        this.dtinicio = dtinicio;
    }

    public Date getDtfim() {
        return dtfim;
    }

    public void setDtfim(Date dtfim) {
        this.dtfim = dtfim;
    }

   
    
    
}
