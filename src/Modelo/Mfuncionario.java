/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Macedo
 */
public class Mfuncionario {
    private int idFunc;
    private String funcionario;
    private String acesso;
    private String senha;

    public Mfuncionario() {
    }

    public Mfuncionario(int idFunc, String funcionario, String acesso, String senha) {
        this.idFunc = idFunc;
        this.funcionario = funcionario;
        this.acesso = acesso;
        this.senha = senha;
    }

    public int getIdFunc() {
        return idFunc;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public String getAcesso() {
        return acesso;
    }

    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
}
