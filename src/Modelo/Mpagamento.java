/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class Mpagamento {
    private int idpag;
    private int idReser;
    private int idfunc;
    private Double valortotal;
    private String tipopag;
    private Date datapag;

    public Mpagamento() {
    }

    public Mpagamento(int idpag, int idReser, int idfunc, Double valortotal, String tipopag, Date datapag) {
        this.idpag = idpag;
        this.idReser = idReser;
        this.idfunc = idfunc;
        this.valortotal = valortotal;
        this.tipopag = tipopag;
        this.datapag = datapag;
    }

   

    public int getIdpag() {
        return idpag;
    }

    public void setIdpag(int idpag) {
        this.idpag = idpag;
    }

    public int getIdReser() {
        return idReser;
    }

    public void setIdReser(int idReser) {
        this.idReser = idReser;
    }

    public int getIdfunc() {
        return idfunc;
    }

    public void setIdfunc(int idfunc) {
        this.idfunc = idfunc;
    }

    

    public Double getValortotal() {
        return valortotal;
    }

    public void setValortotal(Double valortotal) {
        this.valortotal = valortotal;
    }

    public String getTipopag() {
        return tipopag;
    }

    public void setTipopag(String tipopag) {
        this.tipopag = tipopag;
    }

   

    public Date getDatapag() {
        return datapag;
    }

    public void setDatapag(Date datapag) {
        this.datapag = datapag;
    }

    
}