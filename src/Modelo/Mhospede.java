/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class Mhospede {
    private int idHosp;
    private String nomeHosp;
    private String cpfHosp;
    private  Date dtnascHosp;
    private String endereco;
    private String cidadeHosp;
    private String ufHosp;
    private String emailHosp;
    private String telefone;
    private String veiculoHosp;

    public Mhospede() {
    }

    public Mhospede(int idHosp, String nomeHosp, String cpfHosp, Date dtnascHosp, String endereco, String cidadeHosp, String ufHosp, String emailHosp, String telefone, String veiculoHosp) {
        this.idHosp = idHosp;
        this.nomeHosp = nomeHosp;
        this.cpfHosp = cpfHosp;
        this.dtnascHosp = dtnascHosp;
        this.endereco = endereco;
        this.cidadeHosp = cidadeHosp;
        this.ufHosp = ufHosp;
        this.emailHosp = emailHosp;
        this.telefone = telefone;
        this.veiculoHosp = veiculoHosp;
    }

    public String getEmailHosp() {
        return emailHosp;
    }

    public void setEmailHosp(String emailHosp) {
        this.emailHosp = emailHosp;
    }

    

    public int getIdHosp() {
        return idHosp;
    }

    public void setIdHosp(int idHosp) {
        this.idHosp = idHosp;
    }

    public String getNomeHosp() {
        return nomeHosp;
    }

    public void setNomeHosp(String nomeHosp) {
        this.nomeHosp = nomeHosp;
    }

    public String getCpfHosp() {
        return cpfHosp;
    }

    public String getVeiculoHosp() {
        return veiculoHosp;
    }

    public void setVeiculoHosp(String veiculoHosp) {
        this.veiculoHosp = veiculoHosp;
    }

    public void setCpfHosp(String cpfHosp) {
        this.cpfHosp = cpfHosp;
    }

    public Date getDtnascHosp() {
        return dtnascHosp;
    }

    public void setDtnascHosp(Date dtnascHosp) {
        this.dtnascHosp = dtnascHosp;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidadeHosp() {
        return cidadeHosp;
    }

    public void setCidadeHosp(String cidadeHosp) {
        this.cidadeHosp = cidadeHosp;
    }

    public String getUfHosp() {
        return ufHosp;
    }

    public void setUfHosp(String ufHosp) {
        this.ufHosp = ufHosp;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    
    
}
