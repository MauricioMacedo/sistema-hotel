/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class Mentrada {
    private int idEntra;
    private int idProd;
    private int qtdeProd;
    private Double valorUnit;
    private Date dataEntra;
    private String funcionario;

    public Mentrada() {
    }

    public Mentrada(int idEntra, int idProd, int qtdeProd, Double valorUnit, Date dataEntra, String funcionario) {
        this.idEntra = idEntra;
        this.idProd = idProd;
         this.qtdeProd = qtdeProd;
        this.valorUnit = valorUnit;
        this.dataEntra = dataEntra;
        this.funcionario = funcionario;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    
   

    public int getIdEntra() {
        return idEntra;
    }

    public void setIdEntra(int idEntra) {
        this.idEntra = idEntra;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    

    public int getQtdeProd() {
        return qtdeProd;
    }

    public void setQtdeProd(int qtdeProd) {
        this.qtdeProd = qtdeProd;
    }

    public Double getValorUnit() {
        return valorUnit;
    }

    public void setValorUnit(Double valorUnit) {
        this.valorUnit = valorUnit;
    }

    public Date getDataEntra() {
        return dataEntra;
    }

    public void setDataEntra(Date dataEntra) {
        this.dataEntra = dataEntra;
    }
    
    
    
}
