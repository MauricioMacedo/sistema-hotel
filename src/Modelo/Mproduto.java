/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Macedo
 */
public class Mproduto {
    private int idProd;
    private String nomeProd;
    private Double valorProd;
    private String descProd;
    private String unidProduto;

    public Mproduto() {
    }

    public Mproduto(int idProd, String nomeProd, Double valorProd, String descProd, String unidProduto) {
        this.idProd = idProd;
        this.nomeProd = nomeProd;
        this.valorProd = valorProd;
        this.descProd = descProd;
        this.unidProduto = unidProduto;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    public String getNomeProd() {
        return nomeProd;
    }

    public void setNomeProd(String nomeProd) {
        this.nomeProd = nomeProd;
    }

    public Double getValorProd() {
        return valorProd;
    }

    public void setValorProd(Double valorProd) {
        this.valorProd = valorProd;
    }

    public String getDescProd() {
        return descProd;
    }

    public void setDescProd(String descProd) {
        this.descProd = descProd;
    }

    public String getUnidProduto() {
        return unidProduto;
    }

    public void setUnidProduto(String unidProduto) {
        this.unidProduto = unidProduto;
    }
    
    
    
    
}
