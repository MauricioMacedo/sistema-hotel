/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class Mconsumo {
    private int idCons;
    private int idReser;
    private int idProd;
    private String funcionario;
    private int qtdeCons;
    private Double valorCons;
    private String tipoCons;
    private Date dataconsumo;
    private int statusCons;

    public Mconsumo() {
    }

    public Mconsumo(int idCons, int idReser, int idProd, String funcionario, int qtdeCons, Double valorCons, String tipoCons, Date dataconsumo, int statusCons) {
        this.idCons = idCons;
        this.idReser = idReser;
        this.idProd = idProd;
        this.funcionario = funcionario;
        this.qtdeCons = qtdeCons;
        this.valorCons = valorCons;
        this.tipoCons = tipoCons;
        this.dataconsumo = dataconsumo;
        this.statusCons = statusCons;
    }

   

    public int getStatusCons() {
        return statusCons;
    }

    public void setStatusCons(int statusCons) {
        this.statusCons = statusCons;
    }

    public int getIdCons() {
        return idCons;
    }

    public void setIdCons(int idCons) {
        this.idCons = idCons;
    }

    public int getIdReser() {
        return idReser;
    }

    public void setIdReser(int idReser) {
        this.idReser = idReser;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

   

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public int getQtdeCons() {
        return qtdeCons;
    }

    public void setQtdeCons(int qtdeCons) {
        this.qtdeCons = qtdeCons;
    }

    public Double getValorCons() {
        return valorCons;
    }

    public void setValorCons(Double valorCons) {
        this.valorCons = valorCons;
    }

    public String getTipoCons() {
        return tipoCons;
    }

    public void setTipoCons(String tipoCons) {
        this.tipoCons = tipoCons;
    }

  
    public Date getDataconsumo() {
        return dataconsumo;
    }

    public void setDataconsumo(Date dataconsumo) {
        this.dataconsumo = dataconsumo;
    }
    
   
}
