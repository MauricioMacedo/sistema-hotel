/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Macedo
 */
public class Mreserva {
    private int idReser;
    private int idQuarto;
    private int idHosp;
    private int idFunc;
    private int qtdeHosp;
    private Date dataReser;
    private Date dataEntrada;
    private Date dataSaida;
    private Double valorReserva;
    private String statusReser;
    private String obsReser;
    private String tipoReser;

    public Mreserva() {
    }

    public Mreserva(int idReser, int idQuarto, int idHosp, int idFunc, int qtdeHosp, Date dataReser, Date dataEntrada, Date dataSaida, Double valorReserva, String statusReser, String obsReser, String tipoReser) {
        this.idReser = idReser;
        this.idQuarto = idQuarto;
        this.idHosp = idHosp;
        this.idFunc = idFunc;
        this.qtdeHosp = qtdeHosp;
        this.dataReser = dataReser;
        this.dataEntrada = dataEntrada;
        this.dataSaida = dataSaida;
        this.valorReserva = valorReserva;
        this.statusReser = statusReser;
        this.obsReser = obsReser;
        this.tipoReser = tipoReser;
    }

   

    public String getObsReser() {
        return obsReser;
    }

    public void setObsReser(String obsReser) {
        this.obsReser = obsReser;
    }

    public String getTipoReser() {
        return tipoReser;
    }

    public void setTipoReser(String tipoReser) {
        this.tipoReser = tipoReser;
    }

   

    public int getIdReser() {
        return idReser;
    }

    public void setIdReser(int idReser) {
        this.idReser = idReser;
    }

    public int getIdQuarto() {
        return idQuarto;
    }

    public void setIdQuarto(int idQuarto) {
        this.idQuarto = idQuarto;
    }

    public int getIdHosp() {
        return idHosp;
    }

    public void setIdHosp(int idHosp) {
        this.idHosp = idHosp;
    }

    public int getIdFunc() {
        return idFunc;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }

    
    public int getQtdeHosp() {
        return qtdeHosp;
    }

    public void setQtdeHosp(int qtdeHosp) {
        this.qtdeHosp = qtdeHosp;
    }

    public Date getDataReser() {
        return dataReser;
    }

    public void setDataReser(Date dataReser) {
        this.dataReser = dataReser;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
        this.dataSaida = dataSaida;
    }

    public Double getValorReserva() {
        return valorReserva;
    }

    public void setValorReserva(Double valorReserva) {
        this.valorReserva = valorReserva;
    }

    public String getStatusReser() {
        return statusReser;
    }

    public void setStatusReser(String statusReser) {
        this.statusReser = statusReser;
    }
    
    
}
