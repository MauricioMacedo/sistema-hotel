/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import Logica.Lproduto;
import Modelo.Mproduto;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Fproduto extends javax.swing.JInternalFrame {

    /**
     * Creates new form Fproduto
     */
    public Fproduto() {
        initComponents();
        mostrar("");
        desativar();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    private String acao="salvar";
    void mostrar(String buscar){
        try{
        DefaultTableModel modelo;
        Lproduto prod=new Lproduto();
        modelo = prod.mostrar(buscar);
        tbprod.setModel(modelo);
        ocultarColuna();
        lbregistro.setText("Total de Registro: " + Integer.toString(prod.totalregistro));
        }catch(Exception ex){
            JOptionPane.showMessageDialog(rootPane, "Erro ao carregar dados da tabela produto!");
        }    
    }
    
    void ocultarColuna(){
        
        
    }
    void desativar(){
        apagar.setEnabled(false);
        cancelar.setEnabled(false);
        salvar.setEnabled(false);
        valor.setEnabled(false);
        descricao.setEnabled(false);
        valor.setEnabled(false);
         produto.setEnabled(false);
        cbunid.setEnabled(false);
        
        jb1.setVisible(false);
        jb2.setVisible(false);
        jb3.setVisible(false);
        jb4.setVisible(false);
        valor.setVisible(false);
        descricao.setVisible(false);
        valor.setVisible(false);
        produto.setVisible(false);
        cbunid.setVisible(false);
        salvar.setVisible(false);
        cancelar.setVisible(false);
        idprod.setVisible(false);
        idprod.setEnabled(false);
        limpar();
        
    }
     void ativar(){
        apagar.setEnabled(false);
        novo.setEnabled(false);
        cancelar.setEnabled(true);
        salvar.setEnabled(true);
        valor.setEnabled(true);
        descricao.setEnabled(true);
        valor.setEnabled(true);
        cbunid.setEnabled(true);
        produto.setEnabled(true);
        
        jb1.setVisible(true);
        jb2.setVisible(true);
        jb3.setVisible(true);
        jb4.setVisible(true);
        valor.setVisible(true);
        descricao.setVisible(true);
        valor.setVisible(true);
        cbunid.setVisible(true);
        salvar.setVisible(true);
        cancelar.setVisible(true);
        produto.setVisible(true);
        
    }
     void limpar(){
         valor.setText("");
         descricao.setText("");
         produto.setText("");
         pesquisa.setText("");
         salvar.setText("SALVAR");
         novo.setEnabled(true);
         acao="salvar";
         
     }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbprod = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pesquisa = new javax.swing.JTextField();
        filtrar = new javax.swing.JButton();
        apagar = new javax.swing.JButton();
        sair = new javax.swing.JButton();
        lbregistro = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        salvar = new javax.swing.JButton();
        novo = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();
        jb1 = new javax.swing.JLabel();
        jb3 = new javax.swing.JLabel();
        jb2 = new javax.swing.JLabel();
        jb4 = new javax.swing.JLabel();
        cbunid = new javax.swing.JComboBox<>();
        valor = new javax.swing.JTextField();
        descricao = new javax.swing.JTextField();
        idprod = new javax.swing.JTextField();
        produto = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbprod.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbprod.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbprodMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbprod);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 1170, 220));

        jLabel2.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PESQUISAR:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 120, 30));

        jLabel3.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("PRODUTOS");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 1190, 60));

        pesquisa.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        pesquisa.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        pesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pesquisaKeyPressed(evt);
            }
        });
        getContentPane().add(pesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 550, 30));

        filtrar.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        filtrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/buscar.png"))); // NOI18N
        filtrar.setText("FILTRAR");
        filtrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtrarActionPerformed(evt);
            }
        });
        getContentPane().add(filtrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 90, 150, 30));

        apagar.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        apagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/apagar.png"))); // NOI18N
        apagar.setText("APAGAR");
        apagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apagarActionPerformed(evt);
            }
        });
        getContentPane().add(apagar, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 90, 190, 30));

        sair.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        sair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        sair.setText("SAIR");
        sair.setToolTipText("");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });
        getContentPane().add(sair, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 0, 110, -1));

        lbregistro.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        lbregistro.setForeground(new java.awt.Color(255, 255, 255));
        lbregistro.setText("jLabel4");
        getContentPane().add(lbregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 350, 160, 20));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 480, 1170, 10));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 380, 1180, 10));

        salvar.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/salvar.png"))); // NOI18N
        salvar.setText("SALVAR");
        salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarActionPerformed(evt);
            }
        });
        getContentPane().add(salvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 510, 290, 40));

        novo.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        novo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/novo.GIF"))); // NOI18N
        novo.setText("NOVO");
        novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoActionPerformed(evt);
            }
        });
        getContentPane().add(novo, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 510, 310, 40));

        cancelar.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        cancelar.setText("CANCELAR");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });
        getContentPane().add(cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 510, 270, 40));

        jb1.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jb1.setForeground(new java.awt.Color(255, 255, 255));
        jb1.setText("PRODUTO:");
        getContentPane().add(jb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, -1, 30));

        jb3.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jb3.setForeground(new java.awt.Color(255, 255, 255));
        jb3.setText("VALOR:");
        getContentPane().add(jb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 410, -1, 30));

        jb2.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jb2.setForeground(new java.awt.Color(255, 255, 255));
        jb2.setText("UNIDADE:");
        getContentPane().add(jb2, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 410, 70, 30));

        jb4.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jb4.setForeground(new java.awt.Color(255, 255, 255));
        jb4.setText("DESCRIÇÃO:");
        getContentPane().add(jb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 410, 100, 30));

        cbunid.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        cbunid.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "unidade", "grama", "porção", "litro", "jarra", "copo", " " }));
        cbunid.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        getContentPane().add(cbunid, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 410, 140, 30));

        valor.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        valor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        valor.setToolTipText("");
        getContentPane().add(valor, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 410, 150, 30));

        descricao.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        descricao.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        descricao.setToolTipText("");
        getContentPane().add(descricao, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 410, 140, 30));

        idprod.setText("jTextField1");
        getContentPane().add(idprod, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 310, 20, -1));

        produto.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        produto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        produto.setToolTipText("");
        getContentPane().add(produto, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 410, 280, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/azul.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1190, 570));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoActionPerformed
        // TODO add your handling code here:
        ativar();
        limpar();
    }//GEN-LAST:event_novoActionPerformed

    private void salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarActionPerformed
        // TODO add your handling code here:
        if(produto.getText().length()==0){
            JOptionPane.showMessageDialog(rootPane, "Insira o nome do Produto!");
            produto.requestFocus();
        }else if(descricao.getText().length()==0){
            JOptionPane.showMessageDialog(rootPane, "insira a descricão");
            descricao.requestFocus();
        }else if(valor.getText().length()==0){
            JOptionPane.showMessageDialog(rootPane, "insira o valor!");
            valor.requestFocus();
        }else{
            Mproduto dts=new Mproduto();
            Lproduto prod=new Lproduto();
            
            dts.setNomeProd(produto.getText());
            dts.setValorProd(Double.parseDouble(valor.getText()));
            dts.setDescProd(descricao.getText());
            
            int selecionado = cbunid.getSelectedIndex();
            dts.setUnidProduto((String) cbunid.getItemAt(selecionado));
            try {
            if(acao.equals("salvar"))
            {
                
                    if(prod.inserir(dts))
                    {
                        JOptionPane.showMessageDialog(rootPane, "Produto cadastrado com sucesso!!");
                        mostrar("");
                        limpar();
                        
                    }
                
            }else if(acao.equals("editar"))
            {
                dts.setIdProd(Integer.parseInt(idprod.getText()));
               if(prod.editar(dts))
               {
                   JOptionPane.showMessageDialog(rootPane, "Produto editado com sucesso");
                   mostrar("");
                   limpar();
                   desativar();
               }
            }} catch (SQLException ex) {
                   JOptionPane.showMessageDialog(rootPane, ex +"erro");
                }
            
        }
        
    }//GEN-LAST:event_salvarActionPerformed

    private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
        // TODO add your handling code here:
        limpar();
        desativar();
        mostrar("");
        this.dispose();
    }//GEN-LAST:event_sairActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        // TODO add your handling code here:
        limpar();
        desativar();
         acao="salvar";
    }//GEN-LAST:event_cancelarActionPerformed

    private void tbprodMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbprodMouseClicked
        // TODO add your handling code here:
        salvar.setText("ATUALIZAR");
        ativar();
        apagar.setEnabled(false);
        acao="editar";
        int linha = tbprod.rowAtPoint(evt.getPoint());
        
        idprod.setText(tbprod.getValueAt(linha,0).toString());
        produto.setText(tbprod.getValueAt(linha, 1).toString());
        valor.setText(tbprod.getValueAt(linha, 2).toString());
        descricao.setText(tbprod.getValueAt(linha, 3).toString());
        cbunid.setSelectedItem(tbprod.getValueAt(linha, 4).toString());
        
    }//GEN-LAST:event_tbprodMouseClicked

    private void apagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apagarActionPerformed
        // TODO add your handling code here:
//         if(Menu.acess.getText().equals("ADMINISTRADOR")){
//        if(!idprod.getText().equals(""))
//        {
//            int confirmacao = JOptionPane.showConfirmDialog(rootPane, "Deseja apagar este registro? ");
//            
//            if(confirmacao==0){
//                Lproduto exprod= new Lproduto();
//                Mproduto exdts=new Mproduto();
//                
//                cancelar.setEnabled(false);
//                
//                exdts.setIdProd(Integer.parseInt(idprod.getText()));
//                try {
//                    exprod.delete(exdts);
                   JOptionPane.showMessageDialog(rootPane, " Registro deve ser editado e não apagado, caso o registro seja apagado poderá causar danos ao sistema");
                mostrar("");
                ativar();
                novo.setEnabled(true);
                acao="salvar";
                salvar.setText("SALVAR");
//                } catch (SQLException ex) {
//                   JOptionPane.showMessageDialog(rootPane, ex+" produto não apagado!");
//                
//                }
//                
//            }
//        }
//         }else{
//             JOptionPane.showMessageDialog(null, "voce não possui permissão!!");
//         }
    }//GEN-LAST:event_apagarActionPerformed

    private void filtrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtrarActionPerformed
        // TODO add your handling code here:
        mostrar(pesquisa.getText());
    }//GEN-LAST:event_filtrarActionPerformed

    private void pesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pesquisaKeyPressed
        // TODO add your handling code here:
          mostrar(pesquisa.getText());
    }//GEN-LAST:event_pesquisaKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apagar;
    private javax.swing.JButton cancelar;
    private javax.swing.JComboBox<String> cbunid;
    private javax.swing.JTextField descricao;
    private javax.swing.JButton filtrar;
    private javax.swing.JTextField idprod;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel jb1;
    private javax.swing.JLabel jb2;
    private javax.swing.JLabel jb3;
    private javax.swing.JLabel jb4;
    private javax.swing.JLabel lbregistro;
    private javax.swing.JButton novo;
    private javax.swing.JTextField pesquisa;
    private javax.swing.JTextField produto;
    private javax.swing.JButton sair;
    private javax.swing.JButton salvar;
    private javax.swing.JTable tbprod;
    private javax.swing.JTextField valor;
    // End of variables declaration//GEN-END:variables
}
