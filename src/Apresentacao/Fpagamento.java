/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import Logica.Lconsumo;
import Logica.Lemail;
import Logica.Lpagamento;
import Logica.Lquarto;
import Modelo.Mpagamento;
import Modelo.Mquarto;
import Modelo.Mreserva;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Macedo
 */
public class Fpagamento extends javax.swing.JInternalFrame {

Calendar cal = Calendar.getInstance();
private Double reserv=0.0;
private String acao="salvar";
    /**
     * Creates new form Fconsumo
     */
    public Fpagamento() {
        initComponents();
        populaJCombo(); 
//        mostrar("");
         
        desativar();
        dtpagamento.setCalendar(cal);
       
    }
    void desativar(){
        hopedes.setEnabled(false);
        numq.setEnabled(false);
        idfunc.setVisible(false);
        idfunc.setEnabled(false);
        idpag.setEnabled(false);
        idpag.setVisible(false);
        valorpag.setEnabled(true);
        dtpagamento.setEnabled(false);
        apagar.setEnabled(false);
        acao="salvar";
        salvar.setText("INSERIR PAGAMENTO");
        valorpag.setText("");
        
    }
    void mostrar(String buscar){
       try{
        DefaultTableModel modelo;
        DefaultTableModel modelo1;
        
        Lpagamento lpag=new Lpagamento();
        Lconsumo lcons=new Lconsumo();
        modelo= lpag.mostrar(buscar);
        modelo1=lcons.mostrar(buscar);
        tbpag.setModel(modelo);
        tbcons.setModel(modelo1);       
        ocultarColuna();
        lbregistro.setText("Qtde de pagamentos: "+Integer.    toString(lpag.totalRegistro));
        lbregistro1.setText("Qtde de Consumos: "+Integer.toString(lcons.totalRegistro));
        lbtotal.setText("Total consumos R$:"+Double.toString(lcons.totalcons));
        lbtotalpag.setText("Total de pagamentos R$:"+Double.toString(lpag.totalPagar));
        valorpag.setText(Double.toString(lcons.totalcons -lpag.totalPagar));
        reserv=lpag.totalPagar;
       }catch(Exception ex){
           JOptionPane.showMessageDialog(rootPane, ex + " " + buscar);
       }
        
    }
    void ocultarColuna(){
        tbpag.getColumnModel().getColumn(0).setMaxWidth(0);
        tbpag.getColumnModel().getColumn(0).setMinWidth(0);
        tbpag.getColumnModel().getColumn(0).setPreferredWidth(0);
        tbpag.getColumnModel().getColumn(1).setMaxWidth(70);
        tbpag.getColumnModel().getColumn(1).setMinWidth(70);
        tbpag.getColumnModel().getColumn(1).setPreferredWidth(70);
        tbpag.getColumnModel().getColumn(2).setMaxWidth(0);
        tbpag.getColumnModel().getColumn(2).setMinWidth(0);
        tbpag.getColumnModel().getColumn(2).setPreferredWidth(0);
        
        tbpag.getColumnModel().getColumn(4).setMaxWidth(0);
        tbpag.getColumnModel().getColumn(4).setMinWidth(0);
        tbpag.getColumnModel().getColumn(4).setPreferredWidth(0);
        tbpag.getColumnModel().getColumn(5).setMaxWidth(50);
        tbpag.getColumnModel().getColumn(5).setMinWidth(50);
        tbpag.getColumnModel().getColumn(5).setPreferredWidth(50);
        
        tbcons.getColumnModel().getColumn(0).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(0).setMinWidth(0);
        tbcons.getColumnModel().getColumn(0).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(1).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(1).setMinWidth(0);
        tbcons.getColumnModel().getColumn(1).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(2).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(2).setMinWidth(0);
        tbcons.getColumnModel().getColumn(2).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(3).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(3).setMinWidth(0);
        tbcons.getColumnModel().getColumn(3).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(4).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(4).setMinWidth(0);
        tbcons.getColumnModel().getColumn(4).setPreferredWidth(0);
        
    }
     void populaJCombo(){
         cbquarto.removeAllItems();
         String sql ="select q.numQuarto FROM reserva r "
                 + " inner join quarto q "
                 + "on q.idQuarto=r.idQuarto "
                 + " where r.statusReser='hospedada' order by q.numQuarto desc";
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            while(rs.next())
            {
                cbquarto.addItem(rs.getString("numQuarto"));
               
            }
        }catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "erro ao carregar pagamentos na tabela "+ex);
        }
    }
      
     
    private void populaText(String numero){
        String sql ="Select r.idReser,h.nomeHosp,r.valorReserva from reserva r "
                + "inner join hospede h on r.idHosp=h.idHosp inner join quarto q"
                + " on r.idQuarto=q.idQuarto  where statusReser='hospedada' and q.numQuarto='"+ numero +"' order by q.numQuarto asc";
        ResultSet rs;
        PreparedStatement pst;
        try
        {
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                numq.setText(rs.getString("idReser"));
                hopedes.setText(rs.getString("nomeHosp"));
                reserv = rs.getDouble("valorReserva");
                
                mostrar(rs.getString("idReser"));
                
            }
        }catch(Exception ex)
        {
             JOptionPane.showMessageDialog(null, "erro ao distribuir os dados pagamentos nos campos "+ex);
        }
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbquarto = new javax.swing.JComboBox<>();
        numq = new javax.swing.JTextField();
        fechar = new javax.swing.JButton();
        imprimir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        sair = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbpag = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        lbregistro = new javax.swing.JLabel();
        apagar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        idfunc = new javax.swing.JTextField();
        hopedes = new javax.swing.JTextField();
        cbtipo = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        dtpagamento = new com.toedter.calendar.JDateChooser();
        idpag = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        novo = new javax.swing.JButton();
        salvar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbcons = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        lbregistro1 = new javax.swing.JLabel();
        valorpag = new javax.swing.JTextField();
        lbtotal = new javax.swing.JLabel();
        lbtotalpag = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbquarto.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        cbquarto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbquartoMousePressed(evt);
            }
        });
        cbquarto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbquartoActionPerformed(evt);
            }
        });
        getContentPane().add(cbquarto, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 80, 170, 30));

        numq.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        numq.setToolTipText("");
        getContentPane().add(numq, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 80, 30));

        fechar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        fechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/quartospequeno.png"))); // NOI18N
        fechar.setText("FECHAR NOTA");
        fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fecharActionPerformed(evt);
            }
        });
        getContentPane().add(fechar, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 80, 170, -1));

        imprimir.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivopequeno.png"))); // NOI18N
        imprimir.setText("IMPRIMIR");
        imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirActionPerformed(evt);
            }
        });
        getContentPane().add(imprimir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 80, 160, -1));

        jLabel2.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("CÓDIGO");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 90, 30));

        jLabel3.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("QUARTO");
        jLabel3.setToolTipText("");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 70, 30));

        sair.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        sair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        sair.setText("SAIR");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });
        getContentPane().add(sair, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 0, 120, -1));

        tbpag.setFont(new java.awt.Font("Sylfaen", 0, 13)); // NOI18N
        tbpag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbpag.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbpagMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbpag);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 570, 170));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 1190, 10));

        lbregistro.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        lbregistro.setForeground(new java.awt.Color(255, 255, 255));
        lbregistro.setText("000");
        getContentPane().add(lbregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 310, 180, 20));

        apagar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        apagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/apagar.png"))); // NOI18N
        apagar.setText("APAGAR");
        apagar.setToolTipText("");
        apagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apagarActionPerformed(evt);
            }
        });
        getContentPane().add(apagar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 80, 180, -1));

        jLabel4.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("HÓSPEDE:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 380, 90, 30));

        jLabel7.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("VALOR DO PAGAMENTO: R$");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 450, -1, 30));

        jLabel9.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("DATA DO PAGAMENTO:");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 440, 190, 30));

        idfunc.setToolTipText("");
        getContentPane().add(idfunc, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 320, 50, -1));

        hopedes.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        hopedes.setToolTipText("");
        getContentPane().add(hopedes, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 380, 460, 30));

        cbtipo.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        cbtipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CARTAO DE CREDITO", "CARTAO DE DEBITO", "DINHEIRO", "DEPOSITO", "CHEQUE", "CORTESIA", " " }));
        cbtipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbtipoActionPerformed(evt);
            }
        });
        getContentPane().add(cbtipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 380, 180, 30));

        jLabel10.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("TIPO DE PAGAMENTO:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 380, 170, 30));

        dtpagamento.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        getContentPane().add(dtpagamento, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 440, 180, 30));

        idpag.setToolTipText("");
        idpag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                idpagActionPerformed(evt);
            }
        });
        getContentPane().add(idpag, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, 50, -1));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 1180, 10));

        novo.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        novo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        novo.setText("CANCELAR");
        novo.setToolTipText("");
        novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoActionPerformed(evt);
            }
        });
        getContentPane().add(novo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 515, 340, 40));

        salvar.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/ReservaPEQ.png"))); // NOI18N
        salvar.setText("INSERIR PAGAMENTO");
        salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarActionPerformed(evt);
            }
        });
        getContentPane().add(salvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 515, 320, 40));

        tbcons.setFont(new java.awt.Font("Sylfaen", 0, 13)); // NOI18N
        tbcons.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbcons.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbconsMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbcons);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 130, 580, 170));

        jLabel11.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("LANÇAMENTO DE PAGAMENTO POR APARTAMENTO");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 960, 60));

        lbregistro1.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        lbregistro1.setForeground(new java.awt.Color(255, 255, 255));
        lbregistro1.setText("000");
        getContentPane().add(lbregistro1, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 310, 180, 20));

        valorpag.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        valorpag.setText(" ");
        getContentPane().add(valorpag, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 450, 170, 30));

        lbtotal.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        lbtotal.setForeground(new java.awt.Color(255, 255, 255));
        lbtotal.setText("000");
        getContentPane().add(lbtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 310, 180, 20));

        lbtotalpag.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        lbtotalpag.setForeground(new java.awt.Color(255, 255, 255));
        lbtotalpag.setText("000");
        getContentPane().add(lbtotalpag, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 180, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/azul.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1190, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbquartoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbquartoMousePressed
        // TODO add your handling code here:
         
    }//GEN-LAST:event_cbquartoMousePressed

    private void cbquartoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbquartoActionPerformed
        
        String v =(String)cbquarto.getSelectedItem();
         populaText(v);       
          
    }//GEN-LAST:event_cbquartoActionPerformed

    private void idpagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_idpagActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_idpagActionPerformed

    private void salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarActionPerformed
        // TODO add your handling code here:
        Double resul=Double.parseDouble(valorpag.getText());
        if(resul<=0.0){
            JOptionPane.showMessageDialog(null, "<ERRO>\n não pode inserir valores igual ou abaixo de 0");
            valorpag.requestFocus();
        }else{
        if(valorpag.getText().length()==0){
            JOptionPane.showMessageDialog(null, "Insira o valor");
            valorpag.requestFocus();
        }else{
            Mpagamento dts=new Mpagamento();
            Lpagamento lcons=new Lpagamento();
            
            dts.setIdReser(Integer.parseInt(numq.getText()));
            
            dts.setIdfunc(Integer.parseInt(Menu.id.getText()));
            dts.setValortotal(Double.parseDouble(valorpag.getText()));
            
              
            int selecionado = cbquarto.getSelectedIndex();
            
            
            selecionado = cbtipo.getSelectedIndex();
            dts.setTipopag((String)cbtipo.getItemAt(selecionado));
            
            
            if(acao.equals("salvar")){
                try {
                    if(lcons.inserir(dts)){
                        JOptionPane.showMessageDialog(null, "Pagamento adicionado");
                        mostrar(numq.getText());
                        
                    }
                } catch (SQLException ex) {
                     JOptionPane.showMessageDialog(null, ex +"erro Pagamento não adicionado");
                }
            }else if(acao.equals("editar")){
                dts.setIdpag(Integer.parseInt(idpag.getText()));
                cal = dtpagamento.getCalendar();
                int a,m,d;
                d=cal.get(Calendar.DAY_OF_MONTH);
                m=cal.get(Calendar.MONTH);
                a=cal.get(Calendar.YEAR)- 1900;
                dts.setDatapag((new Date(a,m,d)));
                try {
                    if(lcons.editar(dts)){
                        JOptionPane.showMessageDialog(null, "Pagamento Atualizado");
                        mostrar(numq.getText());
                        acao="salvar";
                        novo.requestFocus();
                        salvar.setText("SALVAR PAGAMENTO");
                        
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex +"erro Pagamento não atualizado");
                }
            }
        }
        }
    }//GEN-LAST:event_salvarActionPerformed

    private void tbpagMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbpagMousePressed
        // TODO add your handling code here:
            salvar.setText("ATUALIZAR PAGAMENTO");
            dtpagamento.setEnabled(true);
            apagar.setEnabled(true);
            acao="editar";
            int linha=tbpag.getSelectedRow();
            idpag.setText(tbpag.getValueAt(linha,0).toString());
            valorpag.setText(tbpag.getValueAt(linha, 6).toString());
            cbtipo.setSelectedItem(tbpag.getValueAt(linha, 7).toString());
            dtpagamento.setDate(Date.valueOf(tbpag.getValueAt(linha, 8).toString()));
        
    }//GEN-LAST:event_tbpagMousePressed

    private void apagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apagarActionPerformed
        // TODO add your handling code here:
         if(Menu.acess.getText().equals("ADMINISTRADOR")){
        if(!idpag.getText().equals("")){
            int confirmacao = JOptionPane.showConfirmDialog(rootPane, "Deseja apagar este registro? ");
            
            if(confirmacao==0){
                Lpagamento expag = new Lpagamento();
                Mpagamento exdts = new Mpagamento();
                
                exdts.setIdpag(Integer.parseInt(idpag.getText()));
                try {
                    if(expag.delete(exdts)){
                        JOptionPane.showMessageDialog(rootPane, " Registro apagado com sucesso! ");
                        mostrar(numq.getText());
                        
                        acao="salvar";
                        novo.requestFocus();
                        salvar.setText("SALVAR PAGAMENTO");
                        apagar.setEnabled(false);
                        
                    }else{
                        JOptionPane.showMessageDialog(rootPane, " ERRO ao tentar apagar! ");
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex +"erro Pagamento não apagado");
                }
            
            }
        }
         }
         else{JOptionPane.showMessageDialog(null, "voce não possui permissão!!");}
    }//GEN-LAST:event_apagarActionPerformed

    private void novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoActionPerformed
        // TODO add your handling code here:
        desativar();
        
    }//GEN-LAST:event_novoActionPerformed

    private void tbconsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbconsMousePressed
        // TODO add your handling code here:
        
        JOptionPane.showMessageDialog(null, "Para alterar o consumo deve acessar\n "+""
                + "a pagina tela de lançamento de consumo em Consultas");
    }//GEN-LAST:event_tbconsMousePressed

    private void fecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fecharActionPerformed
        // TODO add your handling code here:
        if(valorpag.getText().equals("0.0")){
             
          enviarEmail(hopedes.getText(),numq.getText(),lbtotalpag.getText()); 
            int confirma = JOptionPane.showConfirmDialog(null, "Deseja fechar o apt?");
            if(confirma==0){
            Mreserva dts= new Mreserva();
            Lpagamento fech= new Lpagamento(); 
            Mquarto dts1=new Mquarto();
            Lquarto quar=new Lquarto();
            
            String qnum =(String) cbquarto.getSelectedItem();
            dts1.setNumQuarto(qnum);
            dts.setIdReser(Integer.parseInt(numq.getText()));
                try {
                    if(fech.mudaSituacaoPag(dts) && quar.desocupar(dts1)){
                        JOptionPane.showMessageDialog(null, "Check-OUT efetuado com sucesso!");
                        JOptionPane.showMessageDialog(null, "Apartamento Disponivel!");
                        populaJCombo();
                    }  } catch (SQLException ex) {
                   JOptionPane.showMessageDialog(null, ex +"erro mudança situação e quarto");
                }
            }
        }else{
            JOptionPane.showMessageDialog(null, "Não esta prontopara fechar");
            valorpag.requestFocus();
        }
    }//GEN-LAST:event_fecharActionPerformed

    private void cbtipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbtipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbtipoActionPerformed
private Connection connection=Menu.mysqli.conectar();
    private void imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imprimirActionPerformed
        // TODO add your handling code here:
        
       
       try{
           Carregando carregar= new Carregando();
        carregar.toFront();
        carregar.setVisible(true);
//            JOptionPane.showMessageDialog(rootPane, "Aguarde 1 minuto porfavor, enquanto banco esta sendo verificado...");
            
            
                
//        JOptionPane.showMessageDialog(rootPane, data);
            
        Map p=new HashMap();
           
      JasperReport relatorio;
      JasperPrint impressao;   
        
         p.put("reserva", Integer.parseInt(numq.getText())); 
         p.put("subTotal",reserv); 
        
         
//        JOptionPane.showMessageDialog(rootPane, parametro);
        
                try{       
                   
           
                        relatorio = JasperCompileManager.compileReport(new File("").getAbsolutePath()+"/src/Relatorios/notaCliente.jrxml");
                        impressao = JasperFillManager.fillReport(relatorio, p, connection);
                        JasperViewer view = new JasperViewer(impressao, false);
                        carregar.dispose();
                        JOptionPane.showMessageDialog(rootPane, "Preparando o relatorio");
                        
                        view.setTitle("Nota do Cliente");
                        view.toFront();
                        view.setVisible(true);
                       
                    }catch (Exception e){
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(rootPane, e+ " Erro na geração do relatorio.");
                                        }
                               
           }catch (Exception ex){
         
            JOptionPane.showMessageDialog(rootPane,ex + " Erro na geração do relatorio @.");
                         
    
     
     }
    }//GEN-LAST:event_imprimirActionPerformed

    private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
        // TODO add your handling code here:
        desativar();
        this.dispose();
    }//GEN-LAST:event_sairActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apagar;
    protected static javax.swing.JComboBox<String> cbquarto;
    private javax.swing.JComboBox<String> cbtipo;
    private com.toedter.calendar.JDateChooser dtpagamento;
    private javax.swing.JButton fechar;
    private javax.swing.JTextField hopedes;
    private javax.swing.JTextField idfunc;
    private javax.swing.JTextField idpag;
    private javax.swing.JButton imprimir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbregistro;
    private javax.swing.JLabel lbregistro1;
    private javax.swing.JLabel lbtotal;
    private javax.swing.JLabel lbtotalpag;
    private javax.swing.JButton novo;
    private javax.swing.JTextField numq;
    private javax.swing.JButton sair;
    private javax.swing.JButton salvar;
    private javax.swing.JTable tbcons;
    private javax.swing.JTable tbpag;
    private javax.swing.JTextField valorpag;
    // End of variables declaration//GEN-END:variables
 private void enviarEmail(String h,String idReser, String valor) {
        String sql="select emailHosp from hospede where nomeHosp='"+h+"'";
        String emailhosp="";
        Carregando carregar= new Carregando();
        carregar.toFront();
        carregar.setVisible(true);
        
        try {
		PreparedStatement st = Menu.con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		
		while(rs.next()){
			
			emailhosp=(rs.getString("emailHosp"));
		}
		st.close();
	}
	catch(Exception e){
		JOptionPane.showMessageDialog(null, "erro ao enviar o email");
	}
        Lemail envia= new Lemail();
        envia.setAssunto("Checkout Efetuado");
        envia.setDestino(emailhosp);
        envia.setMensagem("Pagamento efetuado da reserva "+idReser+" \n com o " + valor+", Obrigado pela preferencia"
                + " Att: administracao");

        try{
            envia.enviar();
            carregar.dispose();
             JOptionPane.showMessageDialog(null, "email enviado " +"  ");;
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, "Erro ao enviar email" +"  "+e);
    }
}
}