/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import static Apresentacao.Menu.centralizaForm;
import static Apresentacao.Menu.plano;
import Logica.Lconsumo;
import Modelo.Mconsumo;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Macedo
 */
public class Fconsumo extends javax.swing.JInternalFrame {

Calendar cal = Calendar.getInstance();
            
private String acao="salvar";
    /**
     * Creates new form Fconsumo
     */
    public Fconsumo() {
        initComponents();
        populaJCombo();
        populaCBproduto();
        
        desativar();
        dtconsumo.setCalendar(cal);
       
    }
    void desativar(){
        hopedes.setEnabled(false);
        numq.setEnabled(false);
        idprod.setVisible(false);
        idprod.setEnabled(false);
        idcons.setEnabled(false);
        idcons.setVisible(false);
        valorprod.setEnabled(true);
        dtconsumo.setEnabled(false);
        apagar.setEnabled(false);
        acao="salvar";
         salvar.setText("INSERIR CONSUMO");
         qtde.setText("");
        
    }
    void mostrar(String buscar) throws SQLException{
        DefaultTableModel modelo;
        
        Lconsumo lcons=new Lconsumo();
        modelo= lcons.mostrar(buscar);
        tbcons.setModel(modelo);
        ocultarColuna();
        lbregistro.setText("quantidade de consumo: "+Integer.toString(lcons.totalRegistro));
        
    }
    void ocultarColuna(){
        tbcons.getColumnModel().getColumn(0).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(0).setMinWidth(0);
        tbcons.getColumnModel().getColumn(0).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(1).setMaxWidth(90);
        tbcons.getColumnModel().getColumn(1).setMinWidth(90);
        tbcons.getColumnModel().getColumn(1).setPreferredWidth(90);
        tbcons.getColumnModel().getColumn(2).setMaxWidth(0);
        tbcons.getColumnModel().getColumn(2).setMinWidth(0);
        tbcons.getColumnModel().getColumn(2).setPreferredWidth(0);
        tbcons.getColumnModel().getColumn(5).setMaxWidth(50);
        tbcons.getColumnModel().getColumn(5).setMinWidth(50);
        tbcons.getColumnModel().getColumn(5).setPreferredWidth(50);
    }
     void populaJCombo(){
         cbquarto.removeAllItems();
         String sql ="select q.numQuarto FROM reserva r "
                 + " inner join quarto q "
                 + "on q.idQuarto=r.idQuarto "
                 + " where r.statusReser='hospedada' order by q.numQuarto desc";
        
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            while(rs.next())
            {
                cbquarto.addItem(rs.getString("numQuarto"));
               
            }
        }catch(Exception ex)
        {
            
        }
    }
     void populaCBproduto(){
         cbproduto.removeAllItems();
        String sql ="Select nomeProd from produto order by nomeProd asc";
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                cbproduto.addItem(rs.getString("nomeProd"));
               
            }
        }catch(Exception ex)
        {
            
        }
    }
    private void populaText(String numero){
        String sql ="Select r.idReser,r.idHosp, h.nomeHosp, r.idQuarto, q.numQuarto from reserva r inner join hospede h on r.idHosp=h.idHosp "
                + " inner join quarto q on r.idQuarto=q.idQuarto "
                + "where r.statusReser='hospedada' and q.numQuarto='"+ numero +"' order by q.numQuarto asc";
        ResultSet rs;
        PreparedStatement pst;
        try
        {
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                numq.setText(rs.getString("idReser"));
                hopedes.setText(rs.getString("nomeHosp"));
                mostrar(rs.getString("idReser"));
                populaCBproduto();
            }
        }catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "erro na entrada de consumo!!"+ ex);
        }
    }
     private void popTextProd(String produ){
        String sql ="Select idProd, valorProd from produto where nomeProd='"+ produ +"'";
        ResultSet rs;
        PreparedStatement pst;
        try
        {
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                idprod.setText(rs.getString("idProd"));
                valorprod.setText(rs.getString("valorProd"));
               
            }
        }catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbquarto = new javax.swing.JComboBox<>();
        sair = new javax.swing.JButton();
        numq = new javax.swing.JTextField();
        cfechar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbcons = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        lbregistro = new javax.swing.JLabel();
        apagar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        idprod = new javax.swing.JTextField();
        hopedes = new javax.swing.JTextField();
        qtde = new javax.swing.JFormattedTextField();
        cbtipo = new javax.swing.JComboBox<>();
        cbproduto = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        dtconsumo = new com.toedter.calendar.JDateChooser();
        idcons = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        novo = new javax.swing.JButton();
        salvar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        valorprod = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbquarto.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        cbquarto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbquartoMousePressed(evt);
            }
        });
        cbquarto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbquartoActionPerformed(evt);
            }
        });
        getContentPane().add(cbquarto, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, 100, 30));

        sair.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        sair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        sair.setText("SAIR");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });
        getContentPane().add(sair, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 0, 100, 30));

        numq.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        numq.setToolTipText("");
        getContentPane().add(numq, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 130, 30));

        cfechar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        cfechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/quartospequeno.png"))); // NOI18N
        cfechar.setText("PAGAMENTOS");
        cfechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cfecharActionPerformed(evt);
            }
        });
        getContentPane().add(cfechar, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 80, 160, 40));

        jButton3.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivopequeno.png"))); // NOI18N
        jButton3.setText("IMPRIMIR");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 80, 160, 40));

        jLabel2.setFont(new java.awt.Font("Sylfaen", 0, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("CÓDIGO");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 80, 30));

        jLabel3.setFont(new java.awt.Font("Sylfaen", 0, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("QUARTO");
        jLabel3.setToolTipText("");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 80, 70, 30));

        tbcons.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbcons.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbconsMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbcons);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 1130, 170));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 1160, 10));

        lbregistro.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbregistro.setForeground(new java.awt.Color(255, 255, 255));
        lbregistro.setText("000");
        getContentPane().add(lbregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 310, 220, 30));

        apagar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        apagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/apagar.png"))); // NOI18N
        apagar.setText("APAGAR");
        apagar.setToolTipText("");
        apagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apagarActionPerformed(evt);
            }
        });
        getContentPane().add(apagar, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 80, 200, 40));

        jLabel4.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("HÓSPEDE:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 110, 40));

        jLabel6.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("PRODUTO:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 380, 100, 40));

        jLabel7.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("VALOR:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 450, 90, 30));

        jLabel8.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("QUANTIDADE:");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 450, 180, 30));

        jLabel9.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("DATA:");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 450, 80, 30));

        idprod.setToolTipText("");
        getContentPane().add(idprod, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 320, 50, -1));

        hopedes.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        hopedes.setToolTipText("");
        getContentPane().add(hopedes, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 380, 380, 40));

        qtde.setToolTipText("");
        qtde.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        getContentPane().add(qtde, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 450, 190, 30));

        cbtipo.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        cbtipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "consumo bar", "consumo frigobar", "restaurante", "diaria" }));
        getContentPane().add(cbtipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 380, 190, 40));

        cbproduto.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        cbproduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbprodutoActionPerformed(evt);
            }
        });
        getContentPane().add(cbproduto, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 380, 240, 40));

        jLabel10.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("TIPO:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 380, 100, 40));

        dtconsumo.setDateFormatString("dd/MM/yyyy HH:mm:ss");
        dtconsumo.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        getContentPane().add(dtconsumo, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 450, 190, 30));

        idcons.setToolTipText("");
        idcons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                idconsActionPerformed(evt);
            }
        });
        getContentPane().add(idcons, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, 50, -1));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 1170, 10));

        novo.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        novo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/cancelar.png"))); // NOI18N
        novo.setText("CANCELAR");
        novo.setToolTipText("");
        novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoActionPerformed(evt);
            }
        });
        getContentPane().add(novo, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 520, 310, 40));

        salvar.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/produtos - Copia.png"))); // NOI18N
        salvar.setText("INSIRIR CONSUMO");
        salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarActionPerformed(evt);
            }
        });
        getContentPane().add(salvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 520, 300, 40));

        jLabel11.setFont(new java.awt.Font("Sylfaen", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("LANÇAMENTO DE CONSUMO POR APARTAMENTO");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1140, 60));

        valorprod.setFont(new java.awt.Font("Sylfaen", 0, 18)); // NOI18N
        getContentPane().add(valorprod, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 450, 190, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/azul.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1170, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbquartoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbquartoMousePressed
        // TODO add your handling code here:
         
    }//GEN-LAST:event_cbquartoMousePressed

    private void cbquartoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbquartoActionPerformed
        String v=(String)cbquarto.getSelectedItem();
         populaText(v);
        
          
    }//GEN-LAST:event_cbquartoActionPerformed

    private void idconsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_idconsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_idconsActionPerformed

    private void cbprodutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbprodutoActionPerformed
        // TODO add your handling code here:
        String v=(String)cbproduto.getSelectedItem();
        popTextProd(v);
    }//GEN-LAST:event_cbprodutoActionPerformed

    private void salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarActionPerformed
        // TODO add your handling code here:
        
        if(qtde.getText().length()==0){
            JOptionPane.showMessageDialog(null, "Insira a quantidade");
            qtde.requestFocus();
        }else{
            Mconsumo dts=new Mconsumo();
            Lconsumo lcons=new Lconsumo();
            
            dts.setIdReser(Integer.parseInt(numq.getText()));
            dts.setIdProd(Integer.parseInt(idprod.getText()));
            
            dts.setFuncionario(Menu.nomefunc.getText());
            dts.setValorCons(Double.parseDouble(valorprod.getText()));
            dts.setQtdeCons(Integer.parseInt(qtde.getText()));
              
            int selecionado = cbtipo.getSelectedIndex();
            dts.setTipoCons((String) cbtipo.getItemAt(selecionado));
            
            
            
                
                int a,m,d;
                d=cal.get(Calendar.DAY_OF_MONTH);
                m=cal.get(Calendar.MONTH);
                a=cal.get(Calendar.YEAR)- 1900;
            if(acao.equals("salvar")){
                try {
                    if(lcons.inserir(dts)){
                        JOptionPane.showMessageDialog(null, "Consumo adicionado");
                        mostrar(numq.getText());
                        desativar();
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,ex+ " erro Consumo não adicionado");
                }
            }else if(acao.equals("editar")){
                dts.setIdCons(Integer.parseInt(idcons.getText()));
                
                dts.setDataconsumo((new Date(a,m,d)));
                try {
                    if(lcons.editar(dts)){
                        JOptionPane.showMessageDialog(null, "Consumo Atualizado");
                        mostrar(numq.getText());
                        desativar();
                        
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,ex+ " erro Consumo não atualizado");
                }
            }
        }
        
    }//GEN-LAST:event_salvarActionPerformed

    private void tbconsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbconsMousePressed
        // TODO add your handling code here:
           
            salvar.setText("ATUALIZAR CONSUMO");
            apagar.setEnabled(true);
            acao="editar";
            int linha=tbcons.getSelectedRow();
            idcons.setText(tbcons.getValueAt(linha,0).toString());
            numq.setText(tbcons.getValueAt(linha, 1).toString());
            idprod.setText(tbcons.getValueAt(linha, 2).toString());
            hopedes.setText(tbcons.getValueAt(linha, 3).toString());
            qtde.setText(tbcons.getValueAt(linha, 5).toString());
            valorprod.setText(tbcons.getValueAt(linha,6).toString());
            cbtipo.setSelectedItem(tbcons.getValueAt(linha,7).toString());
            cbproduto.setSelectedItem(tbcons.getValueAt(linha,8).toString());            
//            dtconsumo.setDate(Date.valueOf(tbcons.getValueAt(linha, 9).toString()));
            
    }//GEN-LAST:event_tbconsMousePressed

    private void apagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apagarActionPerformed
        // TODO add your handling code here:
        if(Menu.acess.getText().equals("ADMINISTRADOR")){
        if(!idcons.getText().equals("")){
            int confirmacao = JOptionPane.showConfirmDialog(rootPane, "Deseja apagar este registro? ");
            
            if(confirmacao==0){
                Lconsumo excon = new Lconsumo();
                Mconsumo exdts = new Mconsumo();
                
                exdts.setIdCons(Integer.parseInt(idcons.getText()));
                try {
                    if(excon.delete(exdts)){
                        JOptionPane.showMessageDialog(rootPane, " Registro apagado com sucesso! ");
                        mostrar(numq.getText());
                        acao="salvar";
                        novo.requestFocus();
                        salvar.setText("SALVAR");
                        apagar.setEnabled(false);
                        qtde.setText("");
                    }else{
                        JOptionPane.showMessageDialog(rootPane, " ERRO ao tentar apagar! ");
                    }
                } catch (SQLException ex) {
                   JOptionPane.showMessageDialog(null,ex+ " erro Consumo não foi apagado");
                }
            
            }
        }
        }else{
            JOptionPane.showMessageDialog(null, "voce não possui permissão!!");
        }
    }//GEN-LAST:event_apagarActionPerformed

    private void novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoActionPerformed
        // TODO add your handling code here:
        desativar();
        acao.equals("salvar");
        salvar.setText("INSERIR CONSUMO");
        qtde.setText("");
        
    }//GEN-LAST:event_novoActionPerformed

    private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
        // TODO add your handling code here:
        desativar();
        this.dispose();
    }//GEN-LAST:event_sairActionPerformed

    private void cfecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cfecharActionPerformed
        // TODO add your handling code here
        
        Fpagamento pg= Menu.pg;
        plano.add(pg);
        centralizaForm(pg);
        Fpagamento.cbquarto.setSelectedItem(cbquarto.getSelectedItem());
        pg.toFront();
        pg.setVisible(true);
        pg.populaJCombo();
        this.dispose();
        
    }//GEN-LAST:event_cfecharActionPerformed
private Connection connection=Menu.con;
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        try{
            JOptionPane.showMessageDialog(rootPane, "Aguarde 1 minuto porfavor, enquanto banco esta sendo verificado...");
            
            
                
//        JOptionPane.showMessageDialog(rootPane, data);
            
        Map p=new HashMap();
           
      JasperReport relatorio;
      JasperPrint impressao;   
        
         p.put("reserva", Integer.parseInt(numq.getText())); 
         
        
         
//        JOptionPane.showMessageDialog(rootPane, parametro);
        
                try{       
                   
           
                        relatorio = JasperCompileManager.compileReport(new File("").getAbsolutePath()+"/src/Relatorios/Notaconsumo.jrxml");
                        impressao = JasperFillManager.fillReport(relatorio, p, connection);
                        JOptionPane.showMessageDialog(rootPane, "Preparando o relatorio");
                        JasperViewer view = new JasperViewer(impressao, false);
                        
                        
                        view.setTitle("Nota do Consumo");
                        view.toFront();
                        view.setVisible(true);
                       
                    }catch (Exception e){
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(rootPane, e+ " Erro na geração do relatorio.");
                                        }
                               
           }catch (Exception ex){
         
            JOptionPane.showMessageDialog(rootPane,ex + " Erro na geração do relatorio @.");
                         
    
     
     }
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apagar;
    private javax.swing.JComboBox<String> cbproduto;
    private javax.swing.JComboBox<String> cbquarto;
    private javax.swing.JComboBox<String> cbtipo;
    private javax.swing.JButton cfechar;
    private com.toedter.calendar.JDateChooser dtconsumo;
    private javax.swing.JTextField hopedes;
    private javax.swing.JTextField idcons;
    private javax.swing.JTextField idprod;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbregistro;
    private javax.swing.JButton novo;
    private javax.swing.JTextField numq;
    private javax.swing.JFormattedTextField qtde;
    private javax.swing.JButton sair;
    private javax.swing.JButton salvar;
    private javax.swing.JTable tbcons;
    private javax.swing.JTextField valorprod;
    // End of variables declaration//GEN-END:variables
}
