/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import static Apresentacao.Menu.centralizaForm;
import static Apresentacao.Menu.plano;
import Logica.Lemail;
import Logica.Lhospede;
import Logica.Lquarto;
import Logica.Lreserva;
import Modelo.Mhospede;
import Modelo.Mquarto;
import Modelo.Mreserva;
import java.awt.Color;
import java.awt.Component;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static java.util.Calendar.getInstance;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
////import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Creserva extends javax.swing.JInternalFrame {
       private  Integer checkin;
       private  Integer confirmada;
       private  Integer somareser;
       private  Integer verifica;
       private Integer vdiaria;
       private String verificada;
       
    /**
     * Creates new form Creserva
     */
    public Creserva() {
        initComponents();
        checkin("");
        confirmada("");
        checkout("");
         soma(); 
       
    }
   private void pacote (String pacote){
    String nomefuncionario=Menu.nomefunc.getText();
    String sSQL="insert into consumo (idReser,idProd,funcionario,qtdeCons,valorCons,tipoCons,dataConsumo)" +
    "select r.idReser,p.idProd,'"+nomefuncionario+"','1',r.valorReserva, 'pacote',now()" +
    "from Reserva r, Produto p where r.idReser='"+pacote+"' and p.nomeProd='pacote'";
     
           try {
               PreparedStatement pst=Menu.con.prepareStatement(sSQL);
               pst.executeUpdate(sSQL);
               
           } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex + "Erro do valor de pacote automaticas");
           }
  }
   private void diaria(String diaria){
    String nomefuncionario=Menu.nomefunc.getText();
    String datahoje = Menu.datahoje.getText();
    String sSQL="insert into consumo (idReser,idProd,funcionario,qtdeCons,valorCons,tipoCons,dataConsumo)" +
    "select r.idReser,p.idProd,'"+nomefuncionario+"','1',r.valorReserva, 'diaria',now()" +
    "from Reserva r, Produto p where r.idReser='"+diaria+"' and p.nomeProd='diaria automatica'";
     String SQLi2="update reserva set autoDiaria='"+datahoje+"' where idReser='"+diaria+"'";
     
           try {
               PreparedStatement pst=Menu.con.prepareStatement(sSQL);
               pst.executeUpdate(sSQL);
               pst=Menu.con.prepareStatement(SQLi2);
                pst.executeUpdate(SQLi2);
               
               
           } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex + "Erro do valor de pacote automaticas");
           }
   }
  private void verifica(String id){
      verifica =0;
      String sql ="Select veiculoHosp from hospede where idHosp='"+id+"' and veiculoHosp='nao possui' order by idHosp asc";
        
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            while(rs.next())
            {
               verifica++;
               
            }
        }catch(Exception ex)
        {
            
        }
  }    
   void soma() {
       somareser=checkin + confirmada;
       lbreserva.setText(Integer.toString(somareser));
           try {
               ok("");
                okSai("");
           } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, ex + " erro na soma");
           }
      
       
   }
   
     private void addCor(JTable table ) {
                
            int a,m,d;
           Calendar c = Calendar.getInstance();
             d=c.get(Calendar.DAY_OF_MONTH);
             m=c.get(Calendar.MONTH);
             a=c.get(Calendar.YEAR) - 1900;
             Date hoje=(new Date(a,m,d));
        
        // minha table se chama jTable1, se precisar troque o nome
        table.setDefaultRenderer(Object.class, new javax.swing.table.DefaultTableCellRenderer() {   
//        @Override
//        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {   
//        super.getTableCellRendererComponent(tbhoje, value, isSelected, hasFocus, row, column);   
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            // aqui você coloca a condição para pintar a linha, neste caso se o valor da coluna 4 da linha for menor que 2 (table.getValueAt(row,4), aqui table é o nome que está no parametro ali em cima, row é a linha atual da verificação)
         
         

          Date estado = Date.valueOf(table.getModel().getValueAt(row, 9).toString());
          Date sai = Date.valueOf(table.getModel().getValueAt(row, 10).toString());
          String situa = table.getModel().getValueAt(row, 12).toString();
        
        if(estado.equals(hoje)){
             //comp.setBackground(Color.geen);
           comp.setBackground(new Color(100, 200, 50));
           
        } else if(situa.equals("confirmada")){
           comp.setBackground(Color.lightGray);
            //comp.setBackground(new Color(170, 170, 170));
//         comp.setBackground(new Color(109, 149, 254));
         
        } else if(estado.before(hoje)){
            comp.setBackground(Color.YELLOW);            
             //comp.setBackground(new Color(255, 91, 96));
//            
       } else{
            comp.setBackground(new Color(170, 170, 170));
            //comp.setBackground(Color.GRAY);
        }
        
        return comp;
 }
        });
    }
     private void addCor2( ) {
                    
            int a,m,d;
           Calendar c = Calendar.getInstance();
             d=c.get(Calendar.DAY_OF_MONTH);
             m=c.get(Calendar.MONTH);
             a=c.get(Calendar.YEAR) - 1900;
             Date hoje=(new Date(a,m,d));
             
             ////////SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            ///////////////////  Date hoje = Date.valueOf(formato.format(hoje1));
        
        // minha table se chama jTable1, se precisar troque o nome
        tbhosp.setDefaultRenderer(Object.class, new javax.swing.table.DefaultTableCellRenderer() {   
         public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            // aqui você coloca a condição para pintar a linha, neste caso se o valor da coluna 4 da linha for menor que 2 (table.getValueAt(row,4), aqui table é o nome que está no parametro ali em cima, row é a linha atual da verificação)
             
          Date estado = Date.valueOf(table.getModel().getValueAt(row, 9).toString());
          Date sai = Date.valueOf(table.getModel().getValueAt(row, 10).toString());
          String situa = table.getModel().getValueAt(row, 12).toString();
              
                
        
        if(estado.equals(hoje)){
             //comp.setBackground(Color.geen);
           comp.setBackground(new Color(109, 149, 254));
           
        } else if(sai.equals(hoje)){
           //comp.setBackground(Color.BLUE);
            //comp.setBackground(new Color(170, 170, 170));
          comp.setBackground(new Color(255, 91, 96));
         
        } else if(sai.before(hoje)){
            comp.setBackground(Color.YELLOW);            
            //            
       } else{
            comp.setBackground(new Color(170, 170, 170));
            //comp.setBackground(Color.GRAY);
        }
        
        return comp;
 }
        });
    }
    
//     void ocultarColuna(){
////        tbcheckin.getColumnModel().getColumn(1).setMaxWidth(0);
////        tbcheckin.getColumnModel().getColumn(1).setMinWidth(0);
////        tbcheckin.getColumnModel().getColumn(1).setPreferredWidth(0);
////        tbcheckin.getColumnModel().getColumn(2).setMaxWidth(0);
////        tbcheckin.getColumnModel().getColumn(2).setMinWidth(0);
////        tbcheckin.getColumnModel().getColumn(2).setPreferredWidth(0);
////        tbcheckin.getColumnModel().getColumn(3).setMaxWidth(0);
////        tbcheckin.getColumnModel().getColumn(3).setMinWidth(0);
////        tbcheckin.getColumnModel().getColumn(3).setPreferredWidth(0);
////        tbcheckin.getColumnModel().getColumn(5).setMaxWidth(0);
////        tbcheckin.getColumnModel().getColumn(5).setMinWidth(0);
////        tbcheckin.getColumnModel().getColumn(5).setPreferredWidth(0);
////        
////         tbconfirmada.getColumnModel().getColumn(1).setMaxWidth(0);
////        tbconfirmada.getColumnModel().getColumn(1).setMinWidth(0);
////        tbconfirmada.getColumnModel().getColumn(1).setPreferredWidth(0);
////        tbconfirmada.getColumnModel().getColumn(2).setMaxWidth(0);
////        tbconfirmada.getColumnModel().getColumn(2).setMinWidth(0);
////        tbconfirmada.getColumnModel().getColumn(2).setPreferredWidth(0);
////        tbconfirmada.getColumnModel().getColumn(3).setMaxWidth(0);
////        tbconfirmada.getColumnModel().getColumn(3).setMinWidth(0);
////        tbconfirmada.getColumnModel().getColumn(3).setPreferredWidth(0);
////        tbconfirmada.getColumnModel().getColumn(5).setMaxWidth(0);
////        tbconfirmada.getColumnModel().getColumn(5).setMinWidth(0);
////        tbconfirmada.getColumnModel().getColumn(5).setPreferredWidth(0);
////        
////        tbhosp.getColumnModel().getColumn(1).setMaxWidth(0);
////        tbhosp.getColumnModel().getColumn(1).setMinWidth(0);
////        tbhosp.getColumnModel().getColumn(1).setPreferredWidth(0);
////        tbhosp.getColumnModel().getColumn(2).setMaxWidth(0);
////        tbhosp.getColumnModel().getColumn(2).setMinWidth(0);
////        tbhosp.getColumnModel().getColumn(2).setPreferredWidth(0);
////        tbhosp.getColumnModel().getColumn(3).setMaxWidth(0);
////        tbhosp.getColumnModel().getColumn(3).setMinWidth(0);
////        tbhosp.getColumnModel().getColumn(3).setPreferredWidth(0);
////        tbhosp.getColumnModel().getColumn(5).setMaxWidth(0);
////        tbhosp.getColumnModel().getColumn(5).setMinWidth(0);
////        tbhosp.getColumnModel().getColumn(5).setPreferredWidth(0);
////        tbhosp.getColumnModel().getColumn(6).setMaxWidth(0);
////        tbhosp.getColumnModel().getColumn(6).setMinWidth(0);
////        tbhosp.getColumnModel().getColumn(6).setPreferredWidth(0);
//        
//        tbvencida.getColumnModel().getColumn(1).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(1).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(1).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(5).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(5).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(5).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(6).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(6).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(6).setPreferredWidth(0);
//    }
    
    public void ok(String check) throws SQLException{
        int a,m,d;
           Calendar c = Calendar.getInstance();
             d=c.get(Calendar.DAY_OF_MONTH);
             m=c.get(Calendar.MONTH);
             a=c.get(Calendar.YEAR) - 1900;
             Date hoje=(new Date(a,m,d));
             
        DefaultTableModel ok;
        Lreserva hos= new Lreserva();
        ok = hos.ok(hoje.toString());
        lbentrada.setText(Integer.toString(hos.totalregistro));
    }
    public void okSai(String check){
        int a,m,d;
           Calendar c = Calendar.getInstance();
             d=c.get(Calendar.DAY_OF_MONTH);
             m=c.get(Calendar.MONTH);
             a=c.get(Calendar.YEAR) - 1900;
             Date hoje=(new Date(a,m,d));
             
        DefaultTableModel ok;
        Lreserva hos= new Lreserva();
        ok = hos.verSaida(hoje.toString());
        lbsaida.setText(Integer.toString(hos.totalregistro));
    }
    public void checkin( String buscar){
        try{
            DefaultTableModel modelo;
           
            Lreserva hos= new Lreserva();
            modelo = hos.checkin(buscar);
            tbcheckin.setModel(modelo);
            addCor(tbcheckin);
//            ocultarColuna();
        tbcheckin.getColumnModel().getColumn(0).setMaxWidth(70);
        tbcheckin.getColumnModel().getColumn(0).setMinWidth(70);
        tbcheckin.getColumnModel().getColumn(0).setPreferredWidth(70);
        tbcheckin.getColumnModel().getColumn(1).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(1).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(1).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(2).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(2).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(2).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(3).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(3).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(3).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(4).setMaxWidth(50);
        tbcheckin.getColumnModel().getColumn(4).setMinWidth(50);
        tbcheckin.getColumnModel().getColumn(4).setPreferredWidth(50);
        tbcheckin.getColumnModel().getColumn(5).setMaxWidth(150);
        tbcheckin.getColumnModel().getColumn(5).setMinWidth(150);
        tbcheckin.getColumnModel().getColumn(5).setPreferredWidth(150);
        tbcheckin.getColumnModel().getColumn(6).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(6).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(6).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(7).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(7).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(7).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(8).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(8).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(8).setPreferredWidth(0);
       
        tbcheckin.getColumnModel().getColumn(12).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(12).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(12).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(13).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(13).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(13).setPreferredWidth(0);
        tbcheckin.getColumnModel().getColumn(14).setMaxWidth(0);
        tbcheckin.getColumnModel().getColumn(14).setMinWidth(0);
        tbcheckin.getColumnModel().getColumn(14).setPreferredWidth(0);
        lbcheckin.setText("Total de Registros: " + Integer.toString(hos.totalregistro));
        checkin= hos.totalregistro;
        
       
        }catch(Exception ex){
        JOptionPane.showMessageDialog(rootPane, "Erro ao carregar dados da tabela checkin " + ex);
            try {
                Menu.con.close();
//                System.exit(0);
            } catch (SQLException ex1) {
                Logger.getLogger(Creserva.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } 
    }
    void confirmada( String buscar){
        try{
            DefaultTableModel modelo;
            Lreserva hos= new Lreserva();
            modelo = hos.confirmada(buscar);
            tbconfirmada.setModel(modelo);
            addCor(tbconfirmada);
//            ocultarColuna();
        tbconfirmada.getColumnModel().getColumn(0).setMaxWidth(70);
        tbconfirmada.getColumnModel().getColumn(0).setMinWidth(70);
        tbconfirmada.getColumnModel().getColumn(0).setPreferredWidth(70);
        tbconfirmada.getColumnModel().getColumn(1).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(1).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(1).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(2).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(2).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(2).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(3).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(3).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(3).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(4).setMaxWidth(60);
        tbconfirmada.getColumnModel().getColumn(4).setMinWidth(60);
        tbconfirmada.getColumnModel().getColumn(4).setPreferredWidth(60);
        tbconfirmada.getColumnModel().getColumn(5).setMaxWidth(130);
        tbconfirmada.getColumnModel().getColumn(5).setMinWidth(130);
        tbconfirmada.getColumnModel().getColumn(5).setPreferredWidth(130);
        tbconfirmada.getColumnModel().getColumn(6).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(6).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(6).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(7).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(7).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(7).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(8).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(8).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(8).setPreferredWidth(0);
       
        tbconfirmada.getColumnModel().getColumn(12).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(12).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(12).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(13).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(13).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(13).setPreferredWidth(0);
        tbconfirmada.getColumnModel().getColumn(14).setMaxWidth(0);
        tbconfirmada.getColumnModel().getColumn(14).setMinWidth(0);
        tbconfirmada.getColumnModel().getColumn(14).setPreferredWidth(0);
            lbconfir.setText("Total de Registros: " + Integer.toString(hos.totalregistro));
            confirmada=hos.totalregistro;
       
            
        }catch(Exception ex){
        JOptionPane.showMessageDialog(rootPane, "Erro ao carregar dados da tabela confirmada");
         System.exit(0);}
    }
    void checkout( String buscar){
        try{
            DefaultTableModel modelo;
            Lreserva hos= new Lreserva();
            modelo = hos.hospedadas(buscar);
            tbhosp.setModel(modelo);
           addCor2();
//            ocultarColuna();
tbhosp.getColumnModel().getColumn(0).setMaxWidth(70);
        tbhosp.getColumnModel().getColumn(0).setMinWidth(70);
        tbhosp.getColumnModel().getColumn(0).setPreferredWidth(70);
        tbhosp.getColumnModel().getColumn(1).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(1).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(1).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(2).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(2).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(2).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(3).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(3).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(3).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(4).setMaxWidth(60);
        tbhosp.getColumnModel().getColumn(4).setMinWidth(60);
        tbhosp.getColumnModel().getColumn(4).setPreferredWidth(60);
        tbhosp.getColumnModel().getColumn(5).setMaxWidth(130);
        tbhosp.getColumnModel().getColumn(5).setMinWidth(130);
        tbhosp.getColumnModel().getColumn(5).setPreferredWidth(130);
        tbhosp.getColumnModel().getColumn(6).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(6).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(6).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(7).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(7).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(7).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(8).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(8).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(8).setPreferredWidth(0);
       
        tbhosp.getColumnModel().getColumn(12).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(12).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(12).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(13).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(13).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(13).setPreferredWidth(0);
        tbhosp.getColumnModel().getColumn(14).setMaxWidth(0);
        tbhosp.getColumnModel().getColumn(14).setMinWidth(0);
        tbhosp.getColumnModel().getColumn(14).setPreferredWidth(0);

            lbcheckout.setText("Total de Registros: " + Integer.toString(hos.totalregistro));
            lbocupado.setText(Integer.toString(hos.totalregistro));
            lbcafe.setText(Integer.toString(hos.cafes));
            lbhospede.setText(Integer.toString(hos.cafes));
        
        }catch(Exception ex){
        JOptionPane.showMessageDialog(rootPane, "Erro ao carregar dados da tabela hospedadas");}
    }
//    void vencida( String buscar){
//        try{
//            DefaultTableModel modelo;
//            Lreserva hos= new Lreserva();
//            modelo = hos.vencida(buscar);
//            tbvencida.setModel(modelo);
//            addCor(tbvencida);
////            ocultarColuna();
//    tbvencida.getColumnModel().getColumn(0).setMaxWidth(40);
//        tbvencida.getColumnModel().getColumn(0).setMinWidth(40);
//        tbvencida.getColumnModel().getColumn(0).setPreferredWidth(40);
//        tbvencida.getColumnModel().getColumn(1).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(1).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(1).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(2).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(3).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(4).setMaxWidth(60);
//        tbvencida.getColumnModel().getColumn(4).setMinWidth(60);
//        tbvencida.getColumnModel().getColumn(4).setPreferredWidth(60);
//        tbvencida.getColumnModel().getColumn(5).setMaxWidth(130);
//        tbvencida.getColumnModel().getColumn(5).setMinWidth(130);
//        tbvencida.getColumnModel().getColumn(5).setPreferredWidth(130);
//        tbvencida.getColumnModel().getColumn(6).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(6).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(6).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(7).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(7).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(7).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(8).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(8).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(8).setPreferredWidth(0);
//       
//        tbvencida.getColumnModel().getColumn(12).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(12).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(12).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(13).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(13).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(13).setPreferredWidth(0);
//        tbvencida.getColumnModel().getColumn(14).setMaxWidth(0);
//        tbvencida.getColumnModel().getColumn(14).setMinWidth(0);
//        tbvencida.getColumnModel().getColumn(14).setPreferredWidth(0);
//            lbvencida.setText("Total de Registros: " + Integer.toString(hos.totalregistro));
//            
//       
//            
//        }catch(Exception ex){
//        JOptionPane.showMessageDialog(rootPane, "Erro ao carregar dados da tabela vencida");}
//    }
            /// criar uma verificação das diarias e criar um laço de repetição em que seja inseridas as diarias por dia 
     public void verificaDiaria(){
        vdiaria=0;      
                    Calendar dataAtual = Calendar.getInstance();  
            dataAtual.add(Calendar.DAY_OF_MONTH, -1);  


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dataFormatada = simpleDateFormat.format(dataAtual.getTime());
          JOptionPane.showMessageDialog(null, dataFormatada);

         
         String sql="select MIN(idReser),autoDiaria from reserva where autoDiaria <='"+dataFormatada+"' and statusReser='hospedada'"; 
         
        
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =Menu.con.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                vdiaria=rs.getInt("idReser");
                verificada=rs.getString("autoDiaria");
               
            }
            atualiza();
            
        }catch(Exception ex)
        {
            
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbcheckin = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbconfirmada = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbhosp = new javax.swing.JTable();
        filtrohosp = new javax.swing.JButton();
        atualizar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        deposito = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txthosp = new javax.swing.JTextField();
        txtcheckin = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        filtrocheckin = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtconfirm = new javax.swing.JTextField();
        filtroconfir = new javax.swing.JButton();
        lbsaida = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lbocupado = new javax.swing.JLabel();
        lbcheckin = new javax.swing.JLabel();
        lbreserva = new javax.swing.JLabel();
        lbcafe = new javax.swing.JLabel();
        lbentrada = new javax.swing.JLabel();
        lbhospede = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnconsumo = new javax.swing.JButton();
        pagar = new javax.swing.JButton();
        lbcheckout = new javax.swing.JLabel();
        lbconfir = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbcheckin.setFont(new java.awt.Font("Sylfaen", 0, 13)); // NOI18N
        tbcheckin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbcheckin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbcheckinMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbcheckin);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 560, 130));

        tbconfirmada.setFont(new java.awt.Font("Sylfaen", 0, 13)); // NOI18N
        tbconfirmada.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbconfirmada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbconfirmadaMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbconfirmada);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, 610, 130));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 270, 1180, 10));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 1180, 10));

        tbhosp.setFont(new java.awt.Font("Sylfaen", 0, 13)); // NOI18N
        tbhosp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbhosp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbhospMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tbhosp);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 560, 120));

        filtrohosp.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        filtrohosp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/buscar.png"))); // NOI18N
        filtrohosp.setText("BUSCAR");
        filtrohosp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtrohospActionPerformed(evt);
            }
        });
        getContentPane().add(filtrohosp, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 310, 110, 30));

        atualizar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        atualizar.setText("ATUALIZAR");
        atualizar.setFocusable(false);
        atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atualizarActionPerformed(evt);
            }
        });
        getContentPane().add(atualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 130, -1));

        jLabel10.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("INFORMAÇÕES");
        jLabel10.setToolTipText("");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 280, 460, 20));

        deposito.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        deposito.setText("DEPOSITO");
        deposito.setFocusable(false);
        deposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                depositoActionPerformed(evt);
            }
        });
        getContentPane().add(deposito, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 10, 120, 30));

        jLabel12.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("RESERVADAS");
        jLabel12.setToolTipText("");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 550, 20));

        jLabel11.setFont(new java.awt.Font("Sylfaen", 1, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("CONSULTAS DE RESERVAS");
        jLabel11.setToolTipText("");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 1190, 40));

        jLabel13.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("CONFIRMADA");
        jLabel13.setToolTipText("");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 50, 610, 20));

        jLabel14.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("HOSPEDADAS");
        jLabel14.setToolTipText("");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 490, 20));
        getContentPane().add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 482, 1190, 10));

        jLabel2.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("QTDE DE APTS PARA ENTRAR HOJE:");
        jLabel2.setToolTipText("");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 420, 220, 20));

        jLabel3.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("PESQUISAR:");
        jLabel3.setToolTipText("");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 100, 30));

        txthosp.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        txthosp.setToolTipText("");
        txthosp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txthospKeyPressed(evt);
            }
        });
        getContentPane().add(txthosp, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 310, 320, 30));

        txtcheckin.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        txtcheckin.setToolTipText("");
        txtcheckin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcheckinKeyPressed(evt);
            }
        });
        getContentPane().add(txtcheckin, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 320, 30));

        jLabel4.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("PESQUISAR:");
        jLabel4.setToolTipText("");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 84, -1, 30));

        filtrocheckin.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        filtrocheckin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/buscar.png"))); // NOI18N
        filtrocheckin.setText("BUSCAR");
        filtrocheckin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtrocheckinActionPerformed(evt);
            }
        });
        getContentPane().add(filtrocheckin, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 80, 110, 30));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 50, 10, 430));

        jLabel5.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("PESQUISAR:");
        jLabel5.setToolTipText("");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 80, 110, 30));

        txtconfirm.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        txtconfirm.setToolTipText("");
        txtconfirm.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtconfirmKeyPressed(evt);
            }
        });
        getContentPane().add(txtconfirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 80, 350, 30));

        filtroconfir.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        filtroconfir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/buscar.png"))); // NOI18N
        filtroconfir.setText("BUSCAR");
        filtroconfir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroconfirActionPerformed(evt);
            }
        });
        getContentPane().add(filtroconfir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 80, 130, 30));

        lbsaida.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbsaida.setForeground(new java.awt.Color(255, 255, 255));
        lbsaida.setText("000");
        lbsaida.setToolTipText("");
        getContentPane().add(lbsaida, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 450, 80, 20));

        jLabel8.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("QTDE DE HÓSPEDES:");
        jLabel8.setToolTipText("");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 310, 130, 20));

        jLabel9.setFont(new java.awt.Font("Sylfaen", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("QTDE DE RESERVAS");
        jLabel9.setToolTipText("");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 310, -1, -1));

        jLabel15.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("QTDE DE CAFÉ DA MANHÃ HOJE:");
        jLabel15.setToolTipText("");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(611, 375, 210, 30));

        jLabel16.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("QTDE DE APTS PARA FECHAR HOJE:");
        jLabel16.setToolTipText("");
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(614, 450, 220, 20));

        jLabel17.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("QTDE DE APARTAMENTOS  OCUPADO:");
        jLabel17.setToolTipText("");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(608, 340, 240, 20));

        lbocupado.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbocupado.setForeground(new java.awt.Color(255, 255, 255));
        lbocupado.setText("000");
        lbocupado.setToolTipText("");
        getContentPane().add(lbocupado, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 340, 80, 20));

        lbcheckin.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        lbcheckin.setForeground(new java.awt.Color(255, 255, 255));
        lbcheckin.setText("000");
        lbcheckin.setToolTipText("");
        getContentPane().add(lbcheckin, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 250, 180, -1));

        lbreserva.setFont(new java.awt.Font("Sylfaen", 3, 14)); // NOI18N
        lbreserva.setForeground(new java.awt.Color(255, 255, 255));
        lbreserva.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbreserva.setText("000");
        lbreserva.setToolTipText("");
        getContentPane().add(lbreserva, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 310, 100, -1));

        lbcafe.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbcafe.setForeground(new java.awt.Color(255, 255, 255));
        lbcafe.setText("000");
        lbcafe.setToolTipText("");
        getContentPane().add(lbcafe, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 380, 80, 30));

        lbentrada.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbentrada.setForeground(new java.awt.Color(255, 255, 255));
        lbentrada.setText("000");
        lbentrada.setToolTipText("");
        getContentPane().add(lbentrada, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 420, 80, 30));

        lbhospede.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        lbhospede.setForeground(new java.awt.Color(255, 255, 255));
        lbhospede.setText("000");
        lbhospede.setToolTipText("");
        getContentPane().add(lbhospede, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 310, 40, 20));

        jPanel2.setBackground(new java.awt.Color(102, 102, 255));

        btnconsumo.setFont(new java.awt.Font("Sylfaen", 1, 16)); // NOI18N
        btnconsumo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/produtos.png"))); // NOI18N
        btnconsumo.setText("Lançar Consumo");
        btnconsumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnconsumoActionPerformed(evt);
            }
        });

        pagar.setFont(new java.awt.Font("Sylfaen", 1, 16)); // NOI18N
        pagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/pagamentos.png"))); // NOI18N
        pagar.setText("Lançar Pagamentos");
        pagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(btnconsumo, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 146, Short.MAX_VALUE)
                .addComponent(pagar, javax.swing.GroupLayout.PREFERRED_SIZE, 422, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnconsumo)
                    .addComponent(pagar))
                .addContainerGap())
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 490, 1070, 60));

        lbcheckout.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        lbcheckout.setForeground(new java.awt.Color(255, 255, 255));
        lbcheckout.setText("000");
        lbcheckout.setToolTipText("");
        getContentPane().add(lbcheckout, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 470, 180, -1));

        lbconfir.setFont(new java.awt.Font("Sylfaen", 0, 11)); // NOI18N
        lbconfir.setForeground(new java.awt.Color(255, 255, 255));
        lbconfir.setText("000");
        lbconfir.setToolTipText("");
        getContentPane().add(lbconfir, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 250, 180, -1));

        jPanel3.setBackground(new java.awt.Color(51, 153, 255));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 400, 20, 20));

        jPanel4.setBackground(new java.awt.Color(0, 153, 51));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 370, 20, 20));

        jPanel5.setBackground(new java.awt.Color(255, 51, 51));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 340, 20, 20));

        jLabel6.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Check-in");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 370, 90, 30));

        jLabel7.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Estáveis ");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 400, 90, 20));

        jLabel18.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Check-out");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 340, 110, 30));

        jPanel6.setBackground(new java.awt.Color(255, 255, 0));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 430, -1, -1));

        jLabel19.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Ultrapassadas");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 430, 120, 20));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/azul.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1190, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbcheckinMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbcheckinMousePressed
        // TODO add your handling code here:
        
        
       try{
        int linha=tbcheckin.getSelectedRow();
      if(evt.getClickCount()==2){
          String data = tbcheckin.getValueAt(linha, 9).toString();
            
             if(data.equals(Menu.datahoje.getText().toString())){///se a data for igual a data atual
                    int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realizar o Check-in? \n "+
                    " de: "+ "" + tbcheckin.getValueAt(linha, 5).toString() );
            
                    if(confirmacao==0){///se for clicado em sim
                       Lreserva reser=new Lreserva();
                       Mreserva dts = new Mreserva();
                       Lquarto lquarto= new Lquarto();
                       Mquarto mquarto = new Mquarto();
                       
                       dts.setIdReser(Integer.parseInt(tbcheckin.getValueAt(linha, 0).toString()));
                       mquarto.setIdQuarto(Integer.parseInt(tbcheckin.getValueAt(linha, 1).toString()));
                       
                       if(lquarto.ocupar(mquarto)){///verificando se quarto esta ocupado
                           
                           if(reser.mudaSituacao(dts)){///mudando o status da reserva
                               
                           JOptionPane.showMessageDialog(null, "Checkin efetuado com sucesso!");
                             enviarEmail(tbcheckin.getValueAt(linha, 2).toString(),tbcheckin.getValueAt(linha, 0).toString(),tbcheckin.getValueAt(linha, 9).toString(),tbcheckin.getValueAt(linha, 10).toString(),
                                    tbcheckin.getValueAt(linha, 11).toString(),tbcheckin.getValueAt(linha, 14).toString()); 
                           if(tbcheckin.getValueAt(linha, 14).toString().equals("pacote")){
                           ///se a a reserva for pacote                           
                               pacote(tbcheckin.getValueAt(linha, 0).toString());
                               JOptionPane.showMessageDialog(null, "gerado valor pacote na reserva: "+tbcheckin.getValueAt(linha, 1).toString());
                                                 
                       }else{
                               JOptionPane.showMessageDialog(null, "gerado valor diaria automatica na reserva: "+tbcheckin.getValueAt(linha, 1).toString());
                               diaria(tbcheckin.getValueAt(linha, 0).toString());//gerando valor diaria na reserva
                           }
                       verifica(tbcheckin.getValueAt(linha, 2).toString());
                          
                           if(verifica>=1){///verificando se possui placa
                           int confirmacao2 = JOptionPane.showConfirmDialog(null, "Adicionar O veiculo do Hospede? \n "+ tbcheckin.getValueAt(linha, 5).toString() );
                            
                            if(confirmacao2==0){///se clicar em sim
                             String placa = JOptionPane.showInputDialog("Digite o veiculo e placa");
                               Lhospede hosp= new Lhospede();
                               Mhospede dtsH = new Mhospede();
                               
                               try{
                               dtsH.setVeiculoHosp(placa);
                               dtsH.setIdHosp(Integer.parseInt(tbcheckin.getValueAt(linha, 2).toString()));
                               if(hosp.atualiza(dtsH)){///atualizando a placa
                               JOptionPane.showMessageDialog(null, "Veiculo addicionado com sucesso");
                            checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();
                             
                           }///fimse atualizando a placa
                              }catch(Exception ex){}
                               checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();
                            }//fimse  confimação2
                           }///fim se verificando a placa 
                            checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();   
                           }//fim se atualizando situ~ção da reserva
                       }else{
                           JOptionPane.showMessageDialog(null, "Apartamento ocupado!");
                       }

            }else{
                                        this.dispose();
                                Freserva cad=Menu.cad;
                                plano.add(cad);
                                centralizaForm(cad);
                                cad.toFront();
                                cad.setVisible(true);
                                cad.ativaPorFora();


                                    String cod;
                                    String valor;

                                   Freserva.idreser.setText(tbcheckin.getValueAt(linha, 0).toString());
                                   Freserva.idquart.setText(tbcheckin.getValueAt(linha, 1).toString());
                                   Freserva.idhosp.setText(tbcheckin.getValueAt(linha, 2).toString());
                                   Freserva.idfunc.setText(tbcheckin.getValueAt(linha, 3).toString());
                                   Freserva.nquarto.setText(tbcheckin.getValueAt(linha, 4).toString());
                                   Freserva.hospe.setText(tbcheckin.getValueAt(linha, 5).toString());
                                   Freserva.funcionario.setText(tbcheckin.getValueAt(linha, 6).toString());
                                   Freserva.cbqtde.setSelectedItem(tbcheckin.getValueAt(linha, 7).toString());
                                   Freserva.datareser.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 8).toString()));
                                   Freserva.dtentrada.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 9).toString()));
                                   Freserva.dtsaida.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 10).toString()));
                                   Freserva.valor.setText(tbcheckin.getValueAt(linha, 11).toString());
                                   Freserva.cbstatus.setSelectedItem(tbcheckin.getValueAt(linha, 12).toString());
                                   Freserva.obs.setText(tbcheckin.getValueAt(linha, 13).toString());
                                   Freserva.cbtipo.setSelectedItem(tbcheckin.getValueAt(linha, 14).toString());
            }
          }
             else{
                    this.dispose();
                  Freserva cad=Menu.cad;
                  plano.add(cad);
                  centralizaForm(cad);
                  cad.toFront();
                  cad.setVisible(true);
                  cad.ativaPorFora();


                      String cod;
                      String valor;

                     Freserva.idreser.setText(tbcheckin.getValueAt(linha, 0).toString());
                     Freserva.idquart.setText(tbcheckin.getValueAt(linha, 1).toString());
                     Freserva.idhosp.setText(tbcheckin.getValueAt(linha, 2).toString());
                     Freserva.idfunc.setText(tbcheckin.getValueAt(linha, 3).toString());
                     Freserva.nquarto.setText(tbcheckin.getValueAt(linha, 4).toString());
                     Freserva.hospe.setText(tbcheckin.getValueAt(linha, 5).toString());
                     Freserva.funcionario.setText(tbcheckin.getValueAt(linha, 6).toString());
                     Freserva.cbqtde.setSelectedItem(tbcheckin.getValueAt(linha, 7).toString());
                     Freserva.datareser.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 8).toString()));
                     Freserva.dtentrada.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 9).toString()));
                     Freserva.dtsaida.setDate(Date.valueOf(tbcheckin.getValueAt(linha, 10).toString()));
                     Freserva.valor.setText(tbcheckin.getValueAt(linha, 11).toString());
                     Freserva.cbstatus.setSelectedItem(tbcheckin.getValueAt(linha, 12).toString());
                     Freserva.obs.setText(tbcheckin.getValueAt(linha, 13).toString());
                     Freserva.cbtipo.setSelectedItem(tbcheckin.getValueAt(linha, 14).toString());
                }
      }
       }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex+" <ERRO>");
        }
    }//GEN-LAST:event_tbcheckinMousePressed

    private void atualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atualizarActionPerformed
        // TODO add your handling code here:
        checkin("");
        confirmada("");
        checkout("");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
          
 // Ou qualquer outra forma que tem
             Calendar horaAtua = getInstance();
             java.util.Date str2 = horaAtua.getTime();
             String str = "00:00:00";
             java.util.Date horaVeri;
             java.util.Date horaAtualiza;
             
           try {
               
               String hora = sdf.format(str2);
               horaVeri = sdf.parse(str);
                horaAtualiza = sdf.parse(hora);
               JOptionPane.showMessageDialog(null, hora);

               Time time = new Time(horaVeri.getTime());
                Menu.age.iniciar();
               if(horaAtualiza.after(time)){
                   verificaDiaria();
       
           }else{
                    verificaDiaria();
               }
           } catch (ParseException ex) {
               Logger.getLogger(Creserva.class.getName()).log(Level.SEVERE, null, ex);
           }

            
        
        
    }//GEN-LAST:event_atualizarActionPerformed

    private void tbconfirmadaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbconfirmadaMousePressed
        // TODO add your handling code here:
        try{
         int linha=tbconfirmada.getSelectedRow();
        if(evt.getClickCount()==2){
            String data = tbconfirmada.getValueAt(linha, 9).toString();
            
             if(data.equals(Menu.datahoje.getText().toString())){
            int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realizar o Check-in? \n "+
                    " de: "+ "" + tbconfirmada.getValueAt(linha, 5).toString() );
            
            if(confirmacao==0){
                Lreserva reser=new Lreserva();
                Mreserva dts = new Mreserva();
                Lquarto lquarto= new Lquarto();
                Mquarto mquarto = new Mquarto();
                dts.setIdReser(Integer.parseInt(tbconfirmada.getValueAt(linha, 0).toString()));
                mquarto.setIdQuarto(Integer.parseInt(tbconfirmada.getValueAt(linha, 1).toString()));
                
                if(lquarto.ocupar(mquarto)){
                           
                           if(reser.mudaSituacao(dts)){
                           JOptionPane.showMessageDialog(null, "Checkin efetuado com sucesso!");
                            enviarEmail(tbconfirmada.getValueAt(linha, 2).toString(),tbconfirmada.getValueAt(linha, 0).toString(),tbconfirmada.getValueAt(linha, 9).toString(),tbcheckin.getValueAt(linha, 10).toString(),
                                    tbconfirmada.getValueAt(linha, 11).toString(),tbconfirmada.getValueAt(linha, 14).toString());  
                           if(tbconfirmada.getValueAt(linha, 14).toString().equals("pacote")){
                           
                               pacote(tbconfirmada.getValueAt(linha, 0).toString());
                               JOptionPane.showMessageDialog(null, "gerado valor pacote na reserva: "+tbconfirmada.getValueAt(linha, 1).toString());
                       }else{
                               JOptionPane.showMessageDialog(null, "gerado valor diaria automatica na reserva: "+tbconfirmada.getValueAt(linha, 1).toString());
                               diaria(tbconfirmada.getValueAt(linha, 0).toString());//gerando valor diaria na reserva
                           }
                       verifica(tbconfirmada.getValueAt(linha, 2).toString());
                           
                           if(verifica>=1){///verificando se possui placa
                           int confirmacao2 = JOptionPane.showConfirmDialog(null, "Adicionar O veiculo do Hospede? \n "+ tbconfirmada.getValueAt(linha, 5).toString() );
                           
                            if(confirmacao2==0){///se clicar em sim
                             String placa = JOptionPane.showInputDialog("Digite o veiculo e placa");
                               Lhospede hosp= new Lhospede();
                               Mhospede dtsH = new Mhospede();
                               
                               dtsH.setVeiculoHosp(placa);
                               dtsH.setIdHosp(Integer.parseInt(tbconfirmada.getValueAt(linha, 2).toString()));
                               if(hosp.atualiza(dtsH)){///atualizando a placa
                               JOptionPane.showMessageDialog(null, "Veiculo addicionado com sucesso");
                            checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();
                           }///fimse atualizando a placa
                              
                               checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();
                            }//fimse  confimação2
                           }///fim se verificando a placa 
                            checkin("");
                             confirmada("");
                             checkout("");
                             soma();
                             Menu.age.iniciar();
                             
                           }
                       }else{
                           JOptionPane.showMessageDialog(null, "Apartamento ocupado!");
                       }
                
            }else{
          this.dispose();
        Freserva cad=Menu.cad;
        plano.add(cad);
        centralizaForm(cad);
        cad.toFront();
        cad.setVisible(true);
        cad.ativaPorFora();
        
       
            String cod;
            String valor;
            
           Freserva.idreser.setText(tbconfirmada.getValueAt(linha, 0).toString());
           Freserva.idquart.setText(tbconfirmada.getValueAt(linha, 1).toString());
           Freserva.idhosp.setText(tbconfirmada.getValueAt(linha, 2).toString());
           Freserva.idfunc.setText(tbconfirmada.getValueAt(linha, 3).toString());
           Freserva.nquarto.setText(tbconfirmada.getValueAt(linha, 4).toString());
           Freserva.hospe.setText(tbconfirmada.getValueAt(linha, 5).toString());
           Freserva.funcionario.setText(tbconfirmada.getValueAt(linha, 6).toString());
           Freserva.cbqtde.setSelectedItem(tbconfirmada.getValueAt(linha, 7).toString());
           Freserva.datareser.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 8).toString()));
           Freserva.dtentrada.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 9).toString()));
           Freserva.dtsaida.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 10).toString()));
           Freserva.valor.setText(tbconfirmada.getValueAt(linha, 11).toString());
           Freserva.cbstatus.setSelectedItem(tbconfirmada.getValueAt(linha, 12).toString());
           Freserva.obs.setText(tbconfirmada.getValueAt(linha, 13).toString());
           Freserva.cbtipo.setSelectedItem(tbconfirmada.getValueAt(linha, 14).toString());
      }
        }else{
          this.dispose();
        Freserva cad=Menu.cad;
        plano.add(cad);
        centralizaForm(cad);
        cad.toFront();
        cad.setVisible(true);
        cad.ativaPorFora();
        
       
            String cod;
            String valor;
            
           Freserva.idreser.setText(tbconfirmada.getValueAt(linha, 0).toString());
           Freserva.idquart.setText(tbconfirmada.getValueAt(linha, 1).toString());
           Freserva.idhosp.setText(tbconfirmada.getValueAt(linha, 2).toString());
           Freserva.idfunc.setText(tbconfirmada.getValueAt(linha, 3).toString());
           Freserva.nquarto.setText(tbconfirmada.getValueAt(linha, 4).toString());
           Freserva.hospe.setText(tbconfirmada.getValueAt(linha, 5).toString());
           Freserva.funcionario.setText(tbconfirmada.getValueAt(linha, 6).toString());
           Freserva.cbqtde.setSelectedItem(tbconfirmada.getValueAt(linha, 7).toString());
           Freserva.datareser.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 8).toString()));
           Freserva.dtentrada.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 9).toString()));
           Freserva.dtsaida.setDate(Date.valueOf(tbconfirmada.getValueAt(linha, 10).toString()));
           Freserva.valor.setText(tbconfirmada.getValueAt(linha, 11).toString());
           Freserva.cbstatus.setSelectedItem(tbconfirmada.getValueAt(linha, 12).toString());
           Freserva.obs.setText(tbconfirmada.getValueAt(linha, 13).toString());
           Freserva.cbtipo.setSelectedItem(tbconfirmada.getValueAt(linha, 14).toString());
      }
        }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex+" <ERRO>");
        }
    }//GEN-LAST:event_tbconfirmadaMousePressed

    private void tbhospMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbhospMousePressed
        // TODO add your handling code here:
        try{
        if(evt.getClickCount()==2){
          this.dispose();
        
        Areserva atua=Menu.atua;
        plano.add(atua);
        centralizaForm(atua);
        atua.toFront();
        atua.setVisible(true);
        atua.ativaPorFora();
        
        int linha=tbhosp.getSelectedRow();
            String cod;
            String valor;
            
           Areserva.idreser.setText(tbhosp.getValueAt(linha, 0).toString());
           Areserva.idquart.setText(tbhosp.getValueAt(linha, 1).toString());
           Areserva.idhosp.setText(tbhosp.getValueAt(linha, 2).toString());
           Areserva.idfunc.setText(tbhosp.getValueAt(linha, 3).toString());
           Areserva.nquarto.setText(tbhosp.getValueAt(linha, 4).toString());
           Areserva.hospe.setText(tbhosp.getValueAt(linha, 5).toString());
           Areserva.funcionario.setText(tbhosp.getValueAt(linha, 6).toString());
           Areserva.cbqtde.setSelectedItem(tbhosp.getValueAt(linha, 7).toString());
           Areserva.datareser.setDate(Date.valueOf(tbhosp.getValueAt(linha, 8).toString()));
           Areserva.dtentrada.setDate(Date.valueOf(tbhosp.getValueAt(linha, 9).toString()));
           Areserva.dtsaida.setDate(Date.valueOf(tbhosp.getValueAt(linha, 10).toString()));
           Areserva.valor.setText(tbhosp.getValueAt(linha, 11).toString());
           Areserva.cbstatus.setSelectedItem(tbhosp.getValueAt(linha, 12).toString());
           Areserva.obs.setText(tbhosp.getValueAt(linha, 13).toString());
           Areserva.cbtipo.setSelectedItem(tbhosp.getValueAt(linha, 14).toString());
      }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex+" <ERRO>");
        }
    }//GEN-LAST:event_tbhospMousePressed

    private void btnconsumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnconsumoActionPerformed
        // TODO add your handling code here:
        
        plano.add(Menu.consumo);
        centralizaForm(Menu.consumo);
        Menu.consumo.toFront();
        Menu.consumo.setVisible(true);
        Menu.consumo.populaJCombo();
        Menu.consumo.populaCBproduto();
        Menu.consumo.desativar();
        this.dispose();
        
    }//GEN-LAST:event_btnconsumoActionPerformed

    private void pagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pagarActionPerformed
        // TODO add your handling code here:
        plano.add(Menu.pg);
        centralizaForm(Menu.pg);
        Menu.pg.toFront();
        Menu.pg.setVisible(true);
        Menu.pg.populaJCombo();
        Menu.pg.desativar();
        this.dispose();
    }//GEN-LAST:event_pagarActionPerformed

    private void depositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_depositoActionPerformed
        // TODO add your handling code here:
        
        plano.add(Menu.dep);
        centralizaForm(Menu.dep);
        Menu.dep.toFront();
        Menu.dep.setVisible(true);
        Menu.dep.desativar();
        
        this.dispose();
       
    }//GEN-LAST:event_depositoActionPerformed

    private void filtrocheckinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtrocheckinActionPerformed
        // TODO add your handling code here:
        checkin(txtcheckin.getText());
    }//GEN-LAST:event_filtrocheckinActionPerformed

    private void filtroconfirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtroconfirActionPerformed
        // TODO add your handling code here:
         confirmada(txtconfirm.getText());
    }//GEN-LAST:event_filtroconfirActionPerformed

    private void filtrohospActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtrohospActionPerformed
        // TODO add your handling code here:
         checkout(txthosp.getText());
    }//GEN-LAST:event_filtrohospActionPerformed

    private void txtcheckinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcheckinKeyPressed
        // TODO add your handling code here:
         checkin(txtcheckin.getText());
    }//GEN-LAST:event_txtcheckinKeyPressed

    private void txtconfirmKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtconfirmKeyPressed
        // TODO add your handling code here:
        confirmada(txtconfirm.getText());
    }//GEN-LAST:event_txtconfirmKeyPressed

    private void txthospKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txthospKeyPressed
        // TODO add your handling code here:
        
        checkout(txthosp.getText());
    }//GEN-LAST:event_txthospKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton atualizar;
    private javax.swing.JButton btnconsumo;
    private javax.swing.JButton deposito;
    private javax.swing.JButton filtrocheckin;
    private javax.swing.JButton filtroconfir;
    private javax.swing.JButton filtrohosp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lbcafe;
    private javax.swing.JLabel lbcheckin;
    private javax.swing.JLabel lbcheckout;
    private javax.swing.JLabel lbconfir;
    private javax.swing.JLabel lbentrada;
    private javax.swing.JLabel lbhospede;
    private javax.swing.JLabel lbocupado;
    private javax.swing.JLabel lbreserva;
    private javax.swing.JLabel lbsaida;
    private javax.swing.JButton pagar;
    private javax.swing.JTable tbcheckin;
    private javax.swing.JTable tbconfirmada;
    private javax.swing.JTable tbhosp;
    private javax.swing.JTextField txtcheckin;
    private javax.swing.JTextField txtconfirm;
    private javax.swing.JTextField txthosp;
    // End of variables declaration//GEN-END:variables

    private void atualiza() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void enviarEmail(String h,String idReser, String dtEntra,String dtsair, String valor, String tipo) {
        String sql="select emailHosp from hospede where idHosp='"+h+"'";
        String emailhosp="";
        
        try {
		PreparedStatement st = Menu.con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		
		while(rs.next()){
			
			emailhosp=(rs.getString("emailHosp"));
		}
		st.close();
	}
	catch(Exception e){
		JOptionPane.showMessageDialog(null, "erro ao enviar o email");
	}
        Lemail envia= new Lemail();
        envia.setAssunto("Checkin Efetuado");
        envia.setDestino(emailhosp);
        envia.setMensagem("Checkin efetuado da reserva codigo:"+idReser+" entre as datas: "+dtEntra+" a "+dtsair+" com o valor R$:" + valor+" por " +tipo+" Att: administracao");

        try{
            envia.enviar();

             JOptionPane.showMessageDialog(null, "email enviado " +"  ");;
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, "Erro ao enviar email" +"  "+e);
    }
    }
}
