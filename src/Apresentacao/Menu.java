/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import Logica.Conexao;
import Logica.GerarTarefasAgendadas;
import Logica.Utilizacao;
import Modelo.utilizacao;
import java.awt.Dimension;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Macedo
 */
public class Menu extends javax.swing.JFrame {
    private static Menu p;
    
    public static Menu getInstancia(){
        if(p == null){
            p =  new Menu();
        }
        return p;
    }
   
    protected static Conexao mysqli=new Conexao();
    protected static Connection con=mysqli.conectar();  
    Fquarto qua = new Fquarto();
    protected static Fhospede hos = new Fhospede();
    Ffuncionario func = new Ffuncionario();
    Fproduto prod=new Fproduto();
    protected static Fentrada entrada=new Fentrada();
    protected static Fconsumo consumo=new Fconsumo();
    protected static Freserva cad=new Freserva();
    protected static Areserva atua=new Areserva();
     protected static Creserva cons= new Creserva();
    protected static Fpagamento pg= new Fpagamento();
    private static Bfechada fec=new Bfechada();
    private static Cestoque est=new Cestoque();
    private static Rconsumo rcons= new Rconsumo();
    private static Rreserva rres = new Rreserva();
    private static Rpagamento relp= new Rpagamento();
    protected static Fdeposito dep = new Fdeposito();
    protected static Tconsumo tcons = new Tconsumo();
     protected static RconsIndiv Rind = new RconsIndiv();
     protected static Frm_Email email=new Frm_Email();
    
    protected static GerarTarefasAgendadas age = new GerarTarefasAgendadas();
    
    
    
    /**
     * Creates new form Menu
     */
    public Menu() {
        initComponents();
        this.setLocationRelativeTo(null);
        JOptionPane.setRootFrame(this); 
        
       logo();
       data();
    }
    
    void data(){
        Calendar dhoje = Calendar.getInstance();
        int y,m,d;
        d=dhoje.get(Calendar.DAY_OF_MONTH);
        m=dhoje.get(Calendar.MONTH);
        y=dhoje.get(Calendar.YEAR) - 1900;
        
        
        datahoje.setText(new Date(y,m,d).toString());
        age.iniciar();
        
        Utilizacao util = new Utilizacao();
        utilizacao utilDts = new utilizacao();
        utilDts.setDtinicio((new Date(y,m,d)));
        if(util.atualiza(utilDts)){}
    }
       
      void verificaTelas(){  
          
        if(qua!=null && qua.isVisible()){
             // Este método coloca o frame invisível porém não o retira da memória.
           
            qua.dispose();
          
          
        }
        if(hos!=null && hos.isVisible()){
            hos.dispose();
        }
        if(func!=null && func.isVisible()){
            func.dispose();
        }
        if(prod!=null && prod.isVisible()){
            prod.dispose();
        }
        if(cad!=null && cad.isVisible()){
            cad.dispose();
        }
        if(cons!=null && cons.isVisible()){
            cons.dispose();
           
        }
        if(consumo!=null && consumo.isVisible()){
            consumo.dispose();
        }
        if(entrada!=null && entrada.isVisible()){
            entrada.dispose();
        }
        if(pg!=null && pg.isVisible()){
            pg.dispose();
        }
        if(fec!=null && fec.isVisible()){
            fec.dispose();
        }
         if(est!=null && est.isVisible()){
            est.dispose();
        }
         if(rcons!=null && rcons.isVisible()){
            rcons.dispose();
        }
         if(rres!=null && rres.isVisible()){
             rres.dispose();
         }
          if(relp!=null && relp.isVisible()){
            relp.dispose();
        }
           if(dep!=null && dep.isVisible()){
            dep.dispose();
        }
           if(atua!=null && atua.isVisible()){
            atua.dispose();
        }
           if(tcons!=null && tcons.isVisible()){
////            tcons.dispose();
        }
           
           if(email!=null && email.isVisible()){
            email.dispose();
        }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    public static void centralizaForm(JInternalFrame frame) {
     
        Dimension desktopSize = plano.getSize();
        Dimension jInternalFrameSize = frame.getSize();
        frame.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
    }
    void logo(){
        try{
            this.setIconImage(ImageIO.read(new File("resources/logohotel.png")));
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "erro na entrada da logo");
        }
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        id = new javax.swing.JLabel();
        nomefunc = new javax.swing.JLabel();
        acess = new javax.swing.JLabel();
        datahoje = new javax.swing.JLabel();
        plano = new javax.swing.JLabel();
        barraMenu = new javax.swing.JMenuBar();
        Minicio = new javax.swing.JMenu();
        Mquarto = new javax.swing.JMenu();
        Mproduto = new javax.swing.JMenu();
        Mhospede = new javax.swing.JMenu();
        btnEmail = new javax.swing.JMenuItem();
        Mreserva = new javax.swing.JMenu();
        cadreserva = new javax.swing.JMenuItem();
        consultareser = new javax.swing.JMenuItem();
        Mestoq = new javax.swing.JMenu();
        Mentrda = new javax.swing.JMenuItem();
        Mestoque = new javax.swing.JMenuItem();
        Mrel = new javax.swing.JMenu();
        relHospe = new javax.swing.JMenuItem();
        relpag = new javax.swing.JMenuItem();
        btnRelres = new javax.swing.JMenuItem();
        ESTOQUE = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        Mconfigura = new javax.swing.JMenu();
        funcion = new javax.swing.JMenuItem();
        retornar = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        Msair = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("SISTEMA HOTEL DO JORRO");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        id.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        id.setForeground(new java.awt.Color(255, 255, 255));
        id.setText("jLabel1");
        getContentPane().add(id, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 580, 80, -1));

        nomefunc.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        nomefunc.setForeground(new java.awt.Color(255, 255, 255));
        nomefunc.setText("jLabel2");
        getContentPane().add(nomefunc, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 580, 210, -1));

        acess.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        acess.setForeground(new java.awt.Color(255, 255, 255));
        acess.setText("jLabel3");
        getContentPane().add(acess, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 580, 170, -1));

        datahoje.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        datahoje.setForeground(new java.awt.Color(255, 255, 255));
        datahoje.setText("jLabel1");
        getContentPane().add(datahoje, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 580, 120, -1));

        plano.setBackground(new java.awt.Color(255, 255, 255));
        plano.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        plano.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/plano.png"))); // NOI18N
        plano.setToolTipText("");
        plano.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        getContentPane().add(plano, new org.netbeans.lib.awtextra.AbsoluteConstraints(-3, 5, 1230, 600));

        barraMenu.setBackground(new java.awt.Color(255, 255, 255));
        barraMenu.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sistema Hotel MAGMA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Sylfaen", 0, 36))); // NOI18N
        barraMenu.setToolTipText("");
        barraMenu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        barraMenu.setFocusable(false);
        barraMenu.setFont(new java.awt.Font("Sylfaen", 0, 12)); // NOI18N

        Minicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/Inicio.png"))); // NOI18N
        Minicio.setText("INICIO");
        Minicio.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Minicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MinicioMouseClicked(evt);
            }
        });
        barraMenu.add(Minicio);

        Mquarto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/quartos.png"))); // NOI18N
        Mquarto.setText("APARTAMENTOS");
        Mquarto.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Mquarto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MquartoMouseClicked(evt);
            }
        });
        barraMenu.add(Mquarto);

        Mproduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/produtos.png"))); // NOI18N
        Mproduto.setText("PRODUTO");
        Mproduto.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Mproduto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MprodutoMouseClicked(evt);
            }
        });
        barraMenu.add(Mproduto);

        Mhospede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/clientes.png"))); // NOI18N
        Mhospede.setText("HOSPEDES");
        Mhospede.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Mhospede.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MhospedeMouseClicked(evt);
            }
        });

        btnEmail.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_END, 0));
        btnEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivopequeno.png"))); // NOI18N
        btnEmail.setText("ENVIAR E-MAIL");
        btnEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmailActionPerformed(evt);
            }
        });
        Mhospede.add(btnEmail);

        barraMenu.add(Mhospede);

        Mreserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/reservas-consumos.png"))); // NOI18N
        Mreserva.setText("RESERVA");
        Mreserva.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N

        cadreserva.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        cadreserva.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        cadreserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivo.png"))); // NOI18N
        cadreserva.setText("CADASTRAR");
        cadreserva.setToolTipText("");
        cadreserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadreservaActionPerformed(evt);
            }
        });
        Mreserva.add(cadreserva);

        consultareser.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        consultareser.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        consultareser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/Consultas.png"))); // NOI18N
        consultareser.setText("CONSULTAR");
        consultareser.setToolTipText("");
        consultareser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultareserActionPerformed(evt);
            }
        });
        Mreserva.add(consultareser);

        barraMenu.add(Mreserva);

        Mestoq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/Reservas.png"))); // NOI18N
        Mestoq.setText("ESTOQUE");
        Mestoq.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N

        Mentrda.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
        Mentrda.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Mentrda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/Sem título.png"))); // NOI18N
        Mentrda.setText("ENTRADA");
        Mentrda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MentrdaActionPerformed(evt);
            }
        });
        Mestoq.add(Mentrda);

        Mestoque.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        Mestoque.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Mestoque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/produtos.png"))); // NOI18N
        Mestoque.setText("ESTOQUE");
        Mestoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MestoqueActionPerformed(evt);
            }
        });
        Mestoq.add(Mestoque);

        barraMenu.add(Mestoq);

        Mrel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivo.png"))); // NOI18N
        Mrel.setText("RELATÓRIOS");
        Mrel.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N

        relHospe.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F9, 0));
        relHospe.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        relHospe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/clientes.png"))); // NOI18N
        relHospe.setText("HOSPEDES");
        relHospe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                relHospeActionPerformed(evt);
            }
        });
        Mrel.add(relHospe);

        relpag.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, 0));
        relpag.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        relpag.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/Reservas.png"))); // NOI18N
        relpag.setText("PAGAMENTOS");
        relpag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                relpagActionPerformed(evt);
            }
        });
        Mrel.add(relpag);

        btnRelres.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F11, 0));
        btnRelres.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        btnRelres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/reservas-consumos.png"))); // NOI18N
        btnRelres.setText("RESERVAS");
        btnRelres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelresActionPerformed(evt);
            }
        });
        Mrel.add(btnRelres);

        ESTOQUE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, 0));
        ESTOQUE.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        ESTOQUE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/pagamentos.png"))); // NOI18N
        ESTOQUE.setText("ESTOQUE");
        ESTOQUE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ESTOQUEActionPerformed(evt);
            }
        });
        Mrel.add(ESTOQUE);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem3.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivo.png"))); // NOI18N
        jMenuItem3.setText("INDIVIDUAL");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        Mrel.add(jMenuItem3);

        barraMenu.add(Mrel);

        Mconfigura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/configurações.png"))); // NOI18N
        Mconfigura.setText("CONFIGURAÇÃO");
        Mconfigura.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N

        funcion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F10, java.awt.event.InputEvent.CTRL_MASK));
        funcion.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        funcion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/usuario 32x32.png"))); // NOI18N
        funcion.setText("FUNCIONARIOS");
        funcion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                funcionMouseClicked(evt);
            }
        });
        funcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionActionPerformed(evt);
            }
        });
        Mconfigura.add(funcion);

        retornar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F11, java.awt.event.InputEvent.CTRL_MASK));
        retornar.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        retornar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/arquivo.png"))); // NOI18N
        retornar.setText("RETORNAR RESERVA");
        retornar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retornarActionPerformed(evt);
            }
        });
        Mconfigura.add(retornar);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/pagamentos.png"))); // NOI18N
        jMenuItem1.setText("TRANSFERIR CONSUMO");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        Mconfigura.add(jMenuItem1);

        barraMenu.add(Mconfigura);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/ajuda.png"))); // NOI18N
        jMenu1.setText("INFORMAÇÕES ");
        jMenu1.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        barraMenu.add(jMenu1);

        Msair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icones/sair.png"))); // NOI18N
        Msair.setText("SAIR");
        Msair.setToolTipText("");
        Msair.setFont(new java.awt.Font("Sylfaen", 0, 14)); // NOI18N
        Msair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MsairMouseClicked(evt);
            }
        });

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        jMenuItem2.setText("SAIR");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        Msair.add(jMenuItem2);

        barraMenu.add(Msair);

        setJMenuBar(barraMenu);
        barraMenu.getAccessibleContext().setAccessibleName("");

        getAccessibleContext().setAccessibleName("SISTEMA DE CADASTRO");
        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void MquartoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MquartoMouseClicked
        // TODO add your handling code here:
     verificaTelas();
    plano.add(qua);
    centralizaForm(qua);
    qua.toFront();
    qua.setVisible(true);
    qua.mostrar("");
    qua.desativar();
    
    }//GEN-LAST:event_MquartoMouseClicked

    private void MentrdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MentrdaActionPerformed
        // TODO add your handling code here:

        verificaTelas();
        plano.add(entrada);
        centralizaForm(entrada);
        entrada.populaProdEnt();
        entrada.toFront();
        entrada.setVisible(true);
        entrada.populaProdEnt();
        
        
    }//GEN-LAST:event_MentrdaActionPerformed

    private void btnRelresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelresActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(rres);
        centralizaForm(rres);
        rres.toFront();
        rres.setVisible(true);
    }//GEN-LAST:event_btnRelresActionPerformed

    private void ESTOQUEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ESTOQUEActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(rcons);
        centralizaForm(rcons);
        rcons.toFront();
        rcons.setVisible(true);
    }//GEN-LAST:event_ESTOQUEActionPerformed

    private void MsairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MsairMouseClicked
        // TODO add your handling code here:
                   
        try { 
            con.close();
            JOptionPane.showMessageDialog(null, "conexão com o banco de dados fechada");
            System.exit(0); 
                     
        } catch (SQLException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "erro com o fechamento \n"
                    + " com conexão com o banco de dados ");
        }

    }//GEN-LAST:event_MsairMouseClicked

    private void MinicioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MinicioMouseClicked
        // TODO add your handling code here:
        
        
                verificaTelas();
         
    }//GEN-LAST:event_MinicioMouseClicked

    private void MhospedeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MhospedeMouseClicked
        // TODO add your handling code here:
        verificaTelas();
        plano.add(hos);
        centralizaForm(hos);
        hos.toFront();
        hos.setVisible(true);
        hos.mostrar("");
        hos.desativar();
        
    }//GEN-LAST:event_MhospedeMouseClicked

    private void funcionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_funcionMouseClicked
        // TODO add your handling code here:
       
    }//GEN-LAST:event_funcionMouseClicked

    private void funcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionActionPerformed
        // TODO add your handling code here:
         verificaTelas();
        plano.add(func);
        centralizaForm(func);
        func.toFront();
        func.setVisible(true);
        func.mostrar("");
    }//GEN-LAST:event_funcionActionPerformed

    private void MprodutoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MprodutoMouseClicked
        // TODO add your handling code here:
       verificaTelas();
       plano.add(prod);
       centralizaForm(prod);
       prod.toFront();
       prod.setVisible(true);
       prod.mostrar("");
       prod.desativar();
    }//GEN-LAST:event_MprodutoMouseClicked

    private void cadreservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadreservaActionPerformed
        // TODO add your handling code here:
        Freserva.funcionario.setText(nomefunc.getText());
        Freserva.idfunc.setText(id.getText());
        verificaTelas();
        plano.add(cad);
        centralizaForm(cad);
        cad.toFront();
        cad.setVisible(true);
        cad.desativar();
        cad.mostrar("");
        
    }//GEN-LAST:event_cadreservaActionPerformed

    private void consultareserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultareserActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(cons);
        centralizaForm(cons);
        cons.toFront();
        cons.setVisible(true);
        cons.checkin("");
                cons.checkout("");
                cons.confirmada("");
                cons.soma();
    }//GEN-LAST:event_consultareserActionPerformed

    private void MestoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MestoqueActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(est);
        centralizaForm(est);
        est.toFront();
        est.setVisible(true);
        est.mostrar("");
    }//GEN-LAST:event_MestoqueActionPerformed

    private void relpagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_relpagActionPerformed
        // TODO add your handling code here:
       verificaTelas();        
        plano.add(relp);
        centralizaForm(relp);
        relp.toFront();
        relp.setVisible(true);
    }//GEN-LAST:event_relpagActionPerformed

    private void retornarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retornarActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(fec);
        centralizaForm(fec);
        fec.toFront();
        fec.setVisible(true);
    }//GEN-LAST:event_retornarActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void relHospeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_relHospeActionPerformed
        // TODO add your handling code here:
        Carregando carregar= new Carregando();
        carregar.toFront();
        carregar.setVisible(true);
        Map p=new HashMap();
                                        JasperReport relatorio;
                                        JasperPrint impressao;   
        
//        JOptionPane.showMessageDialog(rootPane, parametro);
                          try{       
                                    relatorio = JasperCompileManager.compileReport(new File("").getAbsolutePath()+"/src/Relatorios/rel_hospe.jrxml");
                                    impressao = JasperFillManager.fillReport(relatorio, p, con);
                                    JasperViewer view = new JasperViewer(impressao, false);
                                    carregar.dispose();
                                    JOptionPane.showMessageDialog(rootPane, "Preparando o relatorio");
////////                                    view.setAlwaysOnTop(true);
                                    view.setTitle("Relatório de entrada");
                                    view.toFront();
                                    view.setVisible(true);
                                }catch (Exception e){
                                    e.printStackTrace();
                                        JOptionPane.showMessageDialog(rootPane, "Erro na geração do relatorio.");
                                        }                   
        //fim se entrada
    }//GEN-LAST:event_relHospeActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "O sistema hoteleiro consiste, basicamente, no \n"
                                          + "gerenciamento de estadias de hospedes no hotel,\n"
                                          + "controlando desde a reserva de acomodações até o \n"
                                          + "acompanhamento do período de estadia, considerando\n"
                                          + "os diversos tipos de consumo efetuados pelos hóspedes,\n"
                                          + "tais como frigobar, restaurante, consumo na piscina \n"
                                          + "e telefonemas. "+"\n"
                                          + "Desenvolvido por Mauricio Macedo =>Analista de Sistema");
    }//GEN-LAST:event_jMenu1MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        
        verificaTelas();
        plano.add(tcons);
        tcons.toFront();
        centralizaForm(tcons);
        tcons.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
         verificaTelas();
        plano.add(Rind);
        Rind.toFront();
        centralizaForm(Rind);
        Rind.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void btnEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmailActionPerformed
        // TODO add your handling code here:
        verificaTelas();
        plano.add(email);
        email.toFront();
        centralizaForm(email);
        email.setVisible(true);
    }//GEN-LAST:event_btnEmailActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                getInstancia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ESTOQUE;
    private javax.swing.JMenu Mconfigura;
    private javax.swing.JMenuItem Mentrda;
    protected static javax.swing.JMenu Mestoq;
    private javax.swing.JMenuItem Mestoque;
    private javax.swing.JMenu Mhospede;
    private javax.swing.JMenu Minicio;
    private javax.swing.JMenu Mproduto;
    private javax.swing.JMenu Mquarto;
    protected static javax.swing.JMenu Mrel;
    private javax.swing.JMenu Mreserva;
    private javax.swing.JMenu Msair;
    public static javax.swing.JLabel acess;
    private javax.swing.JMenuBar barraMenu;
    private javax.swing.JMenuItem btnEmail;
    private javax.swing.JMenuItem btnRelres;
    private javax.swing.JMenuItem cadreserva;
    private javax.swing.JMenuItem consultareser;
    public static javax.swing.JLabel datahoje;
    protected static javax.swing.JMenuItem funcion;
    public static javax.swing.JLabel id;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    public static javax.swing.JLabel nomefunc;
    protected static javax.swing.JLabel plano;
    private javax.swing.JMenuItem relHospe;
    private javax.swing.JMenuItem relpag;
    protected static javax.swing.JMenuItem retornar;
    // End of variables declaration//GEN-END:variables
}
