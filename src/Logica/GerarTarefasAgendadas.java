/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Apresentacao.Menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;

    /**
     * Classe para manipular a execução de tarefas agendadas automaticamentes
     * @author Jean C Becker
     * @version 1.0
     */
public class GerarTarefasAgendadas {
    boolean info=true;
    int verif=0;
    String verificada="";
    int count = 0;
    Date sth;
    public String data;
    private String passada;
    private Date time;
    Timer timer;
    private String[] registro=new String[0];
     SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
      SimpleDateFormat forhora = new SimpleDateFormat("HH:mm:ss");
//      criando variaveis que irão receber o mysql
    private Conexao mysql=new Conexao();
    private Connection connec=mysql.conectar(); // vai receber o mysql conectar
    private String SQLi="";//variavel global
    /**
     * Método para iniciar a execução das tarefas.
     */
    public void iniciar() {
        
        verificaConsumo();  
        timer = new Timer();
//        Executa tarefa a cada 24 horas a partir da primeira
//               timer.schedule(new tarefasDiarias(),
//                0,
//                1 * 1000 * 60 * 60 * 12 );

//        Executa tarefa todo dia as 6 da manha
	Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 14);
        calendar.set(Calendar.MINUTE,10);        
        calendar.set(Calendar.SECOND, 0);
        time = calendar.getTime();
        
       verificaConsumo();  
       timer.schedule(new tarefasDiarias(), time);
       
    }
     void verificaConsumo(){
        verif=0;      
        Calendar cal;
        int d,m,a;
        cal=Calendar.getInstance();
        d=cal.get(Calendar.DAY_OF_MONTH);
        m=cal.get(Calendar.MONTH);
        a=cal.get(Calendar.YEAR) - 1900;
         sth= new Date(a,m,d);
          data =formato.format(sth);
         System.out.println(data);
         Menu.datahoje.setText(data);
         
         String sql="SELECT DISTINCT idReser FROM reserva where autoDiaria!='"+data+"' and statusReser='hospedada'"; 
         
        
        try
        {
            ResultSet rs;
            PreparedStatement pst;
            
            pst =connec.prepareStatement(sql);
            rs =pst.executeQuery();
           
            
            while(rs.next())
            {
                verif++;
               
            }
        }catch(Exception ex)
        {
            
        }
    }
    
   
    /**
     * Método para interromper a execução das tarefas.
     */
    public void parar() {
        timer.cancel();
    }
    private void verificahorario(){
        
    }
    private void atualiza() {
        if(verif>0){
        String funciona = Menu.nomefunc.getText();
            SQLi = "insert into consumo (idReser,idProd,funcionario,qtdeCons,valorCons,tipoCons,dataConsumo)" +
                "   select r.idReser,p.idProd,'"+funciona+"','1',r.valorReserva, 'diaria',now()"+
                    "from Reserva r, Produto p where r.statusReser='hospedada' and p.nomeProd='diaria automatica' "
                    + "and r.tipoReser='diaria' and r.autoDiaria!='"+data+"'";
                
           String SQLi2="update reserva set autoDiaria='atualizada' where statusReser='hospedada'"; 
            try {
                PreparedStatement diariapst=connec.prepareStatement(SQLi);
                diariapst.executeUpdate(SQLi);
                diariapst=connec.prepareStatement(SQLi2);
                diariapst.executeUpdate(SQLi2);
                JOptionPane.showMessageDialog(null, " inserção de diarias automaticas");
//              
            } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, ex + "Erro na inserção de diarias automaticas");
               System.exit(0);
            }
    }
    }
    
    /**
     * Método que contém as tarefas agendadas que serão executadas.
     */
    class tarefasDiarias extends TimerTask {

        public void run() {
           
            verificaConsumo();
            JOptionPane.showMessageDialog(null, "Carregando o banco no sistema  "+sth+verif+"     \n"+ forhora.format(time));
         if (info==true){
               count=1;}if(verif!=0){
           int resposta = JOptionPane.showConfirmDialog(null,"Deseja inserir diarias automaticas?");
           String funciona = Menu.nomefunc.getText();
          if (resposta == JOptionPane.YES_OPTION) {
//            Aqui ficam as tarefas que vão ser executadas...
            SQLi = "insert into consumo (idReser,idProd,funcionario,qtdeCons,valorCons,tipoCons,dataConsumo)" +
                "   select r.idReser,p.idProd,'"+funciona+"','1',r.valorReserva, 'diaria',now()"+
                    "from Reserva r, Produto p where r.statusReser='hospedada' and p.nomeProd='diaria automatica' and r.tipoReser='diaria' and r.autoDiaria!='"+data+"'";
            String SQLi2="update reserva set autoDiaria='"+data+"' where statusReser='hospedada'";                  
            verificaConsumo();
            try {
                PreparedStatement pst=connec.prepareStatement(SQLi);
                pst.executeUpdate(SQLi);
                pst=connec.prepareStatement(SQLi2);
                pst.executeUpdate(SQLi2);
            } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, ex + "Erro na inserção de diarias automaticas");
               System.exit(0);
            }
           
            } else if (resposta == JOptionPane.NO_OPTION) {
                JOptionPane.showMessageDialog(null, "Diarias automaticas não cadastradas");
            
            }
       }else{
                   info=true;
                   JOptionPane.showMessageDialog(null, "Banco esta sendo verificado "+count);
               }       
       
       
JOptionPane.showMessageDialog(null, "estabilizando banco "+count);
        }
        }
    }
