/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;




import Modelo.Mconsumo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lconsumo {
    
 private Conexao mysql= new Conexao();
 private Connection cn=mysql.conectar();
 private String sSQL=""; 
 public Integer totalRegistro; 
 public Double totalcons;
 
 public DefaultTableModel mostrar(String buscar) throws SQLException{ //para trabalhar as busca.
        
        DefaultTableModel modelo;
       
        String[] titulos = {"CODIGO","COD RESER","COD PROD","HOSPEDE","FUNCIONARIO","QTDE","VALOR","TIPO","PRODUTO","DATA"};
        String[] registro = new String[10];  
        totalRegistro = 0;
        totalcons=0.0;
        
        modelo = new DefaultTableModel(null, titulos);//concatenar os valores do bd
        sSQL = "select  c.idCons,c.idReser,c.idProd,c.funcionario,c.qtdeCons,c.valorCons,c.tipoCons,"
                + "c.dataconsumo,h.nomeHosp,r.idQuarto,q.numQuarto,p.nomeProd FROM consumo c "
                + "inner join reserva r on r.idReser=c.idReser inner join hospede h on h.idHosp=r.idHosp "
                + "inner join quarto q on q.idQuarto=r.idQuarto inner join produto p on p.idProd=c.idProd  "
                + "where c.idReser='"+buscar+"' order by c.idReser desc";
        
        
        //passar os campos de cadastro para o banco de dados bd
        try{ 
            Statement st = cn.createStatement();
            ResultSet  rs=st.executeQuery(sSQL); 
            while(rs.next()){
                registro [0]=rs.getString("idCons");
                registro [1]=rs.getString("idReser");
                registro [2]=rs.getString("idProd");
                registro [3]=rs.getString("nomeHosp");
                registro [4]=rs.getString("funcionario");
                registro [5]=rs.getString("qtdeCons");
                registro [6]=rs.getString("valorCons");
                registro [7]=rs.getString("tipoCons");
                registro [8]=rs.getString("nomeProd");
                registro [9]=rs.getString("dataconsumo");
                
                                             
                //laço para sempre que adicionar um quarto gerar mais um registro
                totalRegistro = totalRegistro +1;
                totalcons = totalcons + (rs.getDouble("valorCons")* rs.getInt("qtdeCons"));
                modelo.addRow(registro);
            }
            return modelo; // retornar os resultados
        }catch (Exception e){  // retorna o erro e a informação 
            JOptionPane.showConfirmDialog(null, e+"erro em logica consumo");
            return null;            
        }finally{
            cn.close();
        }
                
    }
 public boolean inserir (Mconsumo dts) throws SQLException{
        sSQL = "insert into consumo(idReser,idProd, funcionario,qtdeCons,valorCons,tipoCons,dataconsumo)" +
                "values(?,?,?,?,?,?,now())";
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser()); 
            pst.setInt(2, dts.getIdProd()); 
            pst.setString(3, dts.getFuncionario()); 
            pst.setInt(4, dts.getQtdeCons());
            pst.setDouble(5, dts.getValorCons());
            pst.setString(6, dts.getTipoCons()); 
           
            
                                     
                       
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e);
            return false;
                    
        }finally{
            cn.close();
        }
    } 
  public boolean editar (Mconsumo dts) throws SQLException{
        sSQL = "update consumo set idReser=?, idProd=?, funcionario=?,qtdeCons=?,valorCons=?,tipoCons=?" +
                "where idCons=?";
        try{
             PreparedStatement pst=cn.prepareStatement(sSQL);
              
            pst.setInt(1, dts.getIdReser()); 
            pst.setInt(2, dts.getIdProd()); 
      
            pst.setString(3, dts.getFuncionario()); 
            pst.setInt(4, dts.getQtdeCons());
            pst.setDouble(5, dts.getValorCons());
            pst.setString(6, dts.getTipoCons()); 
           
            
            pst.setInt(7, dts.getIdCons());
                               
            
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e);
            return false;
                    
        }
        finally{
            cn.close();
        }
    } 
  public boolean delete (Mconsumo dts) throws SQLException{
        sSQL="delete from consumo where idCons=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdCons());
             int n=pst.executeUpdate();
            if (n!=0){
                return true; 
            }else{
                return false;
            }
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
             return false;
        }finally{
            cn.close();
        }
    }
  public boolean muda (Mconsumo dts) throws SQLException{
        sSQL="update consumo set statusCons where idCons=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getStatusCons());
             int n=pst.executeUpdate();
            if (n!=0){
                return true; 
            }else{
                return false;
            }
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
             return false;
        }finally{
            cn.close();
        }
    }
}
