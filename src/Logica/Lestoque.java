/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lestoque {
 private Conexao mysql= new Conexao();
 private Connection cn=mysql.conectar();
 private String sSQL=""; 
 public Integer totalRegistro; 
 
 
  public DefaultTableModel mostrar(String buscar) throws SQLException{ //para trabalhar as busca.
        
        DefaultTableModel modelo;
       
        String[] titulos = {"CODIGO","CODIGO PRODUTO","QUANTIDADE","PRODUTO"};
        String[] registro = new String[4];  
        totalRegistro = 0;
        
        modelo = new DefaultTableModel(null, titulos);//concatenar os valores do bd
        sSQL = "SELECT *,p.nomeProd from estoque e INNER join produto p on e.idProd=p.idProd where p.nomeProd!='pacote' and p.nomeProd!='diaria automatica'"
                + " and p.nomeProd like '%"+buscar+"%'";
        
        
        //passar os campos de cadastro para o banco de dados bd
        try{ 
            Statement st = cn.createStatement();
            ResultSet  rs=st.executeQuery(sSQL); 
            while(rs.next()){
                registro [0]=rs.getString("idEst");
                registro [1]=rs.getString("idProd");
                registro [2]=rs.getString("qtde");
                registro [3]=rs.getString("nomeProd");
                                               
                //laço para sempre que adicionar um quarto gerar mais um registro
                totalRegistro = totalRegistro +1;
                modelo.addRow(registro);
            }
            return modelo; // retornar os resultados
        }catch (Exception e){  // retorna o erro e a informação 
            JOptionPane.showConfirmDialog(null, e);
            return null;            
        }finally{
            cn.close();
        }
                
    
}
}
