/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Modelo.Mhospede;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lhospede {
    private Conexao mysql=new Conexao();
    private Connection cn=mysql.conectar();
    private String sSQL="";
    public Integer totalregistro;
    
    public DefaultTableModel mostrar(String buscar) throws SQLException{
    
    DefaultTableModel modelo;
    String[] titulos ={"COD","HOSPEDE","CPF","NASCIMENTO", "ENDEREÇO","CIDADE","EMAIL","UF","TELEFONE","VEICULO"};
    String[] registro = new String[10];
    totalregistro = 0;
    
    modelo = new DefaultTableModel(null, titulos);
    sSQL = "select * from hospede where nomeHosp like '%" + buscar + "%' or  cpfHosp like '%" + buscar + "%' OR veiculoHosp like '%" + buscar + "%' order by idHosp desc";
    
    
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next()){
                registro[0]=rs.getString("idHosp");
                registro[1]=rs.getString("nomeHosp");
                registro[2]=rs.getString("cpfHosp");
                registro[3]=rs.getString("dtnascHosp");
                registro[4]=rs.getString("enderecoHosp");
                registro[5]=rs.getString("cidadeHosp");                
                registro[6]=rs.getString("emailHosp");
                registro[7]=rs.getString("ufHosp");
                registro[8]=rs.getString("telefone");
                registro[9]=rs.getString("veiculoHosp");
                
                totalregistro = totalregistro+1;
                modelo.addRow(registro);
            }
            return modelo;
        }catch (SQLException ex) {
             JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de hospedes");
             return null;
        }finally{
            cn.close();
        }
              
       
    }
    
    public boolean inserir (Mhospede dts) throws SQLException{
        sSQL= "insert into hospede (nomeHosp, cpfHosp, dtnascHosp, enderecoHosp, cidadeHosp, emailHosp,ufHosp, telefone,veiculoHosp)" +
                "values(?,?,?,?,?,?,?,?,?)";
        
        try{
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getNomeHosp());
            pst.setString(2, dts.getCpfHosp());
            pst.setDate(3, (Date) dts.getDtnascHosp());
            pst.setString(4, dts.getEndereco());
            pst.setString(5, dts.getCidadeHosp());            
            pst.setString(6, dts.getEmailHosp());
            pst.setString(7, dts.getUfHosp());
            pst.setString(8, dts.getTelefone());
            pst.setString(9, dts.getVeiculoHosp());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        
        }catch(Exception ex){
            JOptionPane.showConfirmDialog(null, ex + "Erro ao inserir \"\\n cpf ja existente\"");
        }finally{
            cn.close();
        }
        return false;
    }
    public boolean editar(Mhospede dts) throws SQLException{
        sSQL ="update hospede set nomeHosp=?, cpfHosp=?, dtnascHosp=?, enderecoHosp=?, cidadeHosp=?,emailHosp=?, ufHosp=?, telefone=?, veiculoHosp=? "+
                "where idHosp=?";
      
        try{
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getNomeHosp());
            pst.setString(2, dts.getCpfHosp());
            pst.setDate(3, (Date) dts.getDtnascHosp());
            pst.setString(4, dts.getEndereco());
            pst.setString(5, dts.getCidadeHosp());            
            pst.setString(6, dts.getEmailHosp());
            pst.setString(7, dts.getUfHosp());
            pst.setString(8, dts.getTelefone());
            pst.setString(9, dts.getVeiculoHosp());
            pst.setInt(10, dts.getIdHosp());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
            }catch(Exception ex){
            JOptionPane.showConfirmDialog(null, ex + "\"\\n cpf ja existente\"");
             return false;}finally{
            cn.close();
        }
       
    }
    
    public boolean delete(Mhospede dts){
        sSQL= "delete from hospede where idHosp=?";
        
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdHosp());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
        JOptionPane.showConfirmDialog(null, ex + " Falha ao deletar");
        return false;
        }
        
    }
    public boolean verifica(String cpf) throws SQLException{
        sSQL= "select * from hospede where cpfHosp='%" + cpf + "%'" ;
        
        try{
        Statement st = cn.createStatement();
        ResultSet rs = st.executeQuery(sSQL);
             while(rs.next()){
              String cpfhospede = rs.getString("cpfHosp");
               if (cpfhospede.equals(cpf)){
                   JOptionPane.showMessageDialog(null, "CPF ja existente");
                 st.close();
                 totalregistro = 1;
                }else{
                   totalregistro = 0;
               }
              
        
    }
        }catch(Exception ex){
        JOptionPane.showMessageDialog(null, "Erro ao verificar CPF");
    }finally{
            cn.close();
        }
        return false;
        
    }
    public boolean atualiza(Mhospede dts) throws SQLException{
        sSQL ="update hospede set veiculoHosp=? "+
                "where idHosp=?";
      
        try{
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getVeiculoHosp());
            pst.setInt(2, dts.getIdHosp());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
            }catch(Exception ex){
            JOptionPane.showConfirmDialog(null, ex + "\"\n erro na inserção da placa");
             return false;}finally{
            cn.close();
        }
       
    }

}
