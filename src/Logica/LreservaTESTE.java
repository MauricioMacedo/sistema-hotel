///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Logica;
//
//import Modelo.Mreserva;
//import java.sql.Connection;
//import java.sql.Date;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.text.SimpleDateFormat;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.swing.JOptionPane;
//import javax.swing.table.DefaultTableModel;
//
///**
// *
// * @author Macedo
// */
//public class LreservaTESTE {
//    private Conexao mysql=new Conexao();
//    private Connection cn=mysql.conectar();
//    private String sSQL="";
//    public Integer totalregistro;
//    public Integer cafes;
//    
//    
//    public DefaultTableModel mostrar (String buscar) throws SQLException{
//       DefaultTableModel modelo;
//       String[] titulo={"COD","QUARTO","HOSPEDE","FUNCIONARIO","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
//       String[] registro=new String[10];
//       totalregistro=0;
//       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select idReser,quarto,hospede,funcionario,dataEntrada,"
//               + "dataSaida,valorReserva,statusReser,obsReser,tipoReser from reserva where statusReser like '%" + buscar + "%' and statusReser!='fechada'order by dataEntrada asc";
//       
//       
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//                registro[0]=rs.getString("idReser");
//                registro[1]=rs.getString("quarto");
//                registro[2]=rs.getString("hospede");
//                registro[3]=rs.getString("funcionario");
//                registro[4]=rs.getString("dataEntrada");
//                registro[5]=rs.getString("dataSaida");
//                registro[6]=rs.getString("valorReserva");
//                registro[7]=rs.getString("statusReser");
//                registro[8]=rs.getString("obsReser");
//                registro[9]=rs.getString("tipoReser");
//                
//                totalregistro=totalregistro+1;
//                modelo.addRow(registro);
//            }
//            return modelo;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de entrada");
//             return null;
//        }finally{
//            cn.close();
//        }
//    } 
//    
//        public DefaultTableModel ok (String buscar) throws SQLException{
//       DefaultTableModel ok;     
//       String[] titulo={"TIPO"};
//       String[] registro=new String[1];
//       totalregistro=0;       
//       ok = new DefaultTableModel(null,titulo);
//       sSQL="select idReser from reserva where dataEntrada='"+buscar+"' and statusReser='reservada' order by dataEntrada asc";
//            
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//               registro[0]=rs.getString("idReser");
//                totalregistro=totalregistro+1;
//               ok.addRow(registro);
//            }
//            return ok;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de verificação de check-out");
//             return null;
//        }finally{
//            cn.close();
//        }
//    } 
//         public DefaultTableModel verSaida (String buscar){
//       DefaultTableModel ok;     
//       String[] titulo={"TIPO"};
//       String[] registro=new String[1];
//       totalregistro=0;       
//       ok = new DefaultTableModel(null,titulo);
//       sSQL="select idReser from reserva where dataSaida='"+buscar+"' and statusReser='hospedada' order by dataEntrada asc";
//            
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//               registro[0]=rs.getString("idReser");
//                totalregistro=totalregistro+1;
//               ok.addRow(registro);
//            }
//            return ok;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de verificação de check-out");
//             return null;
//        }
//    } 
//    public DefaultTableModel checkin (String buscar){
//        
//       DefaultTableModel modelo;
//       String[] titulo={"COD","ID APT","ID HOSP","ID FUNC","APT","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
//       String[] registro=new String[15];
//       totalregistro=0;
//       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select * from reserva where (hospede like '%" + buscar + "%'and statusReser='reservada' and autoDiaria!='confirmada') or " 
//               + "( dataEntrada like '%" + buscar + "%'and statusReser='reservada' and autoDiaria!='confirmada') order by dataEntrada asc";
//       
//       Statement st;
//        try {
//            st = cn.createStatement();       
//            ResultSet rs = st.executeQuery(sSQL);
//
//// Adicionando as colunas
//           
//               while (rs.next()) {
////Registra os valor do banco na tabela
//
//Date minhaData = rs.getDate("dataEntrada"); 
//SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
//String data_formatada = formato.format(minhaData);
//minhaData = Date.valueOf(data_formatada);
//
//
//
//
//modelo.addRow(new String[]{
//    //oficial rs.getString("previsaoentrega"),
////data_formatada((rs.getString("previsaoentrega")),
//    
//    
//    registro[0]=rs.getString("idReser"),
//    registro[1]=rs.getString("idQuarto"),
//    registro[2]=rs.getString("idHosp"),
//    registro[3]=rs.getString("idFunc"),
//    registro[4]=rs.getString("quarto"),
//    registro[5]=rs.getString("hospede"),
//    registro[6]=rs.getString("funcionario"),
//    registro[7]=rs.getString("qtdeHosp"),
//    registro[8]=rs.getString("dataReser"),
//    registro[9]=rs.getString("dataEntrada"),
//    registro[10]=rs.getString("dataSaida"),
//    registro[11]=rs.getString("valorReserva"),
//    registro[12]=rs.getString("statusReser"),
//    registro[13]=rs.getString("obsReser"),
//    registro[14]=rs.getString("tipoReser"),
//    
//});
//
////
////DefaultTableModel model;
////try {
////model = modelo;
////TabelaSequencia.setModel(model);
////} catch (Exception ex) {
////JOptionPane.showMessageDialog(null, ex.getMessage());
////}
//
//
//
//return modelo;
//               }  }  catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de entrada");
//             return null;
//        }
//        return null;
//    
//    }
//    
//     public DefaultTableModel confirmada (String buscar){
//       DefaultTableModel modelo;
//       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","QUARTO","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
//       String[] registro=new String[15];
//       totalregistro=0;
//       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select * from reserva where (autoDiaria='confirmada' and  statusReser='reservada') or (hospede like '%" + buscar + "%' and autoDiaria='confirmada' and  statusReser='reservada' )order by dataEntrada desc";
//       
//       
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//                registro[0]=rs.getString("idReser");
//                registro[1]=rs.getString("idQuarto");
//                registro[2]=rs.getString("idHosp");
//                registro[3]=rs.getString("idFunc");
//                registro[4]=rs.getString("quarto");
//                registro[5]=rs.getString("hospede");
//                registro[6]=rs.getString("funcionario");
//                registro[7]=rs.getString("qtdeHosp");
//                registro[8]=rs.getString("dataReser");
//                registro[9]=rs.getString("dataEntrada");
//                registro[10]=rs.getString("dataSaida");
//                registro[11]=rs.getString("valorReserva");
//                registro[12]=rs.getString("statusReser");
//                registro[13]=rs.getString("obsReser");
//                registro[14]=rs.getString("tipoReser");
//                
//                totalregistro=totalregistro+1;
//                modelo.addRow(registro);
//            }
//            return modelo;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de confirmada");
//             return null;
//        }
//    }
//      public DefaultTableModel hospedadas (String buscar){
//       DefaultTableModel modelo;
//       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","QUARTO","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
//       String[] registro=new String[15];
//       totalregistro=0;
//       cafes=0;
//       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select * from reserva where dataEntrada like '%" + buscar + "%' and statusReser='hospedada' or hospede like '%" + buscar + "%' and statusReser='hospedada' order by dataSaida asc";
//       
//       
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//                registro[0]=rs.getString("idReser");
//                registro[1]=rs.getString("idQuarto");
//                registro[2]=rs.getString("idHosp");
//                registro[3]=rs.getString("idFunc");
//                registro[4]=rs.getString("quarto");
//                registro[5]=rs.getString("hospede");
//                registro[6]=rs.getString("funcionario");
//                registro[7]=rs.getString("qtdeHosp");
//                registro[8]=rs.getString("dataReser");
//                registro[9]=rs.getString("dataEntrada");
//                registro[10]=rs.getString("dataSaida");
//                registro[11]=rs.getString("valorReserva");
//                registro[12]=rs.getString("statusReser");
//                registro[13]=rs.getString("obsReser");
//                registro[14]=rs.getString("tipoReser");
//                
//                totalregistro=totalregistro+1;
//                cafes = cafes + (rs.getInt("qtdeHosp"));
//                modelo.addRow(registro);
//            }
//            return modelo;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de hospedadas");
//             return null;
//        }
//        
//    }
//       public DefaultTableModel fechada (String buscar){
//       DefaultTableModel modelo;
//       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","QUARTO","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
//       String[] registro=new String[15];
//       totalregistro=0;
//       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select * from reserva where idReser='"+ buscar +"' and statusReser='fechada' order by dataEntrada desc";
//       
//       
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//                registro[0]=rs.getString("idReser");
//                registro[1]=rs.getString("idQuarto");
//                registro[2]=rs.getString("idHosp");
//                registro[3]=rs.getString("idFunc");
//                registro[4]=rs.getString("quarto");
//                registro[5]=rs.getString("hospede");
//                registro[6]=rs.getString("funcionario");
//                registro[7]=rs.getString("qtdeHosp");
//                registro[8]=rs.getString("dataReser");
//                registro[9]=rs.getString("dataEntrada");
//                registro[10]=rs.getString("dataSaida");
//                registro[11]=rs.getString("valorReserva");
//                registro[12]=rs.getString("statusReser");
//                registro[13]=rs.getString("obsReser");
//                registro[14]=rs.getString("tipoReser");
//                
//                totalregistro=totalregistro+1;
//                modelo.addRow(registro);
//            }
//            return modelo;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de vencida");
//             return null;
//        }
//    }
//    public boolean inserir(Mreserva dts)
//    {
//       sSQL="insert into reserva (idQuarto,idHosp,idFunc,quarto,hospede,funcionario,qtdeHosp,dataReser,dataEntrada,dataSaida,valorReserva,statusReser,obsReser,tipoReser)"+
//               "values(?,?,?,?,?,?,?,now(),?,?,?,?,?,?)"; 
//       
//        try {
//            PreparedStatement pst=cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getIdQuarto());
//            pst.setInt(2, dts.getIdHosp());
//            pst.setInt(3, dts.getIdFunc());
//            pst.setString(4, dts.getQuarto());
//            pst.setString(5, dts.getHospede());
//            pst.setString(6, dts.getFuncionario());
//            pst.setInt(7, dts.getQtdeHosp());
//            pst.setDate(8, dts.getDataEntrada());
//            pst.setDate(9, dts.getDataSaida());
//            pst.setDouble(10, dts.getValorReserva());
//            pst.setString(11, dts.getStatusReser());
//            pst.setString(12, dts.getObsReser());
//            pst.setString(13, dts.getTipoReser());
//            
//            int n=pst.executeUpdate();
//            if(n!=0){
//                return true;
//            }else{
//                return false;
//            }
//            
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex+ "Erro ao inserir");
//            return false;
//        }
//       
//    }
//    public boolean editar(Mreserva dts){
//        sSQL="update reserva set idQuarto=?, idHosp=?, idFunc=?,quarto=?,hospede=?,funcionario=?,qtdeHosp=?,dataReser=?,dataEntrada=?,dataSaida=?,valorReserva=?, statusReser=?,obsReser=?,tipoReser=?"+
//             "where idReser=?";
//        
//        try {
//            PreparedStatement pst=cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getIdQuarto());
//            pst.setInt(2, dts.getIdHosp());
//            pst.setInt(3, dts.getIdFunc());
//            pst.setString(4, dts.getQuarto());
//            pst.setString(5, dts.getHospede());
//            pst.setString(6, dts.getFuncionario());
//            pst.setInt(7, dts.getQtdeHosp());
//            pst.setDate(8,dts.getDataReser());
//            pst.setDate(9, dts.getDataEntrada());
//            pst.setDate(10, dts.getDataSaida());
//            pst.setDouble(11, dts.getValorReserva());
//            pst.setString(12, dts.getStatusReser());
//            pst.setString(13, dts.getObsReser());
//            pst.setString(14, dts.getTipoReser());
//            pst.setInt(15,dts.getIdReser());
//            
//            int n=pst.executeUpdate();
//            if(n!=0){
//                return true;
//            }else{
//                return false;
//            }
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex+ "Erro ao editar");
//            return false;        }
//        
//    }
//    public boolean delete(Mreserva dts){
//        sSQL="delete from reserva where idReser=?";
//        
//       
//        try {
//            
//             PreparedStatement pst = cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getIdReser());
//        int n=pst.executeUpdate();
//            if(n!=0){
//                return true;
//            }else{
//                return false;
//            }
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex+ "Erro ao deletar");
//            return false; 
//       
//    }
//}
//    public boolean mudaSituacao(Mreserva dts){
//        sSQL="update reserva set statusReser='hospedada' where idReser=?";
//        
//       
//        try {
//            
//             PreparedStatement pst = cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getIdReser());
//        int n=pst.executeUpdate();
//            if(n!=0){
//                return true;
//            }else{
//                return false;
//            }
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex+ "Erro ao mudar situação reserva");
//            return false; 
//       
//    }
//}
//     public boolean mudaConfirma(Mreserva dts){
//        sSQL="update reserva set autoDiaria='confirmada' where idReser=?";
//        
//       
//        try {
//            
//             PreparedStatement pst = cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getIdReser());
//        int n=pst.executeUpdate();
//            if(n!=0){
//                return true;
//            }else{
//                return false;
//            }
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex+ "Erro ao mudar situação reserva");
//            return false; 
//       
//    }
//}
//     void fechar(){
//        try {
//            cn.close();
//        } catch (SQLException ex) {
//            Logger.getLogger(LreservaTESTE.class.getName()).log(Level.SEVERE, null, ex);
//        }
//     }
//    
//}
