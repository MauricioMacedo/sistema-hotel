/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Modelo.Mfuncionario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lfuncionario {
    private Conexao mysql= new Conexao();
    private Connection cn=mysql.conectar();
    private String sSQL="";
    public Integer totalregistro;
    
    public DefaultTableModel mostrar(String buscar) throws SQLException{
        
            DefaultTableModel modelo;
            String[] titulos ={"ID","FUNCIONÁRIO","ACESSO","SENHA"};
            String[] registro=new String[4];
            totalregistro=0;
            
            modelo = new DefaultTableModel(null, titulos);
            sSQL = "select idFunc,funcionario,acesso,AES_DECRYPT(senha,'kamaur') as senha from funcionario where funcionario like '%" + buscar +"%' order by idFunc";
            
        try {    
            Statement st =cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next()){
                registro[0]=rs.getString("idFunc");
                registro[1]=rs.getString("funcionario");
                registro[2]=rs.getString("acesso");
                registro[3]=rs.getString("senha");
                
                totalregistro=totalregistro + 1;
                modelo.addRow(registro);
            }
            return modelo;
        } catch (SQLException ex) {
           JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de funcionário");
           return null;
        }finally{
            cn.close();
        }
    }
    public boolean inserir(Mfuncionario dts) throws SQLException{
        sSQL="insert into funcionario (funcionario,acesso, senha) values (?,?,AES_ENCRYPT(?, 'kamaur'))";
        
        try{
            PreparedStatement pst =cn.prepareStatement(sSQL);
            pst.setString(1, dts.getFuncionario());
            pst.setString(2, dts.getAcesso());
            pst.setString(3, dts.getSenha());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showConfirmDialog(null, ex + " Erro ao inserir funcionario \"\\n senha ja existente\"");
        }finally{
            cn.close();
        }
        return false;
        
    }
    public boolean editar(Mfuncionario dts) throws SQLException{
       
            sSQL="update funcionario set funcionario=?, acesso=?,senha=AES_ENCRYPT(?, 'kamaur') where idFunc=?";
        try {     
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setString(1, dts.getFuncionario());
            pst.setString(2, dts.getAcesso());
            pst.setString(3, dts.getSenha());
            pst.setInt(4, dts.getIdFunc());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch(SQLException ex) {
             JOptionPane.showMessageDialog(null, ex + "\n senha ja existente" );
          
           
        }finally{
            cn.close();
        }
         return false;
    }
    public boolean delete(Mfuncionario dts) throws SQLException{
      
            sSQL="delete from funcionario where idFunc=?";
              try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdFunc());
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Falha ao deletar funcionário");
            return true;
        }finally{
            cn.close();
        }
    }
     public DefaultTableModel login( String senha) throws SQLException{
        
    DefaultTableModel modelo;
    String[] titulos = {"ID", "FUNCIONARIO", "ACESSO", "Senha"};
    String[] registro = new String[4];
    totalregistro = 0;
    
    modelo = new DefaultTableModel(null, titulos);
   sSQL = "select idFunc,acesso,funcionario,senha from funcionario where CAST(AES_DECRYPT(senha,'kamaur') as char)='" + senha + "'";
    
    try{
        Statement st = cn.createStatement();
        ResultSet rs=st.executeQuery(sSQL);
        while(rs.next()){
               registro [0]=rs.getString("idFunc");               
               registro [1]=rs.getString("funcionario");
               registro [2]=rs.getString("acesso");
               registro [3]=rs.getString("senha");
              
                
               totalregistro = totalregistro +1;
               modelo.addRow(registro);
        }
        return modelo;
    }catch (Exception e){
        JOptionPane.showConfirmDialog(null, e);
        return null;
    }finally{
            cn.close();
        }
}
     public boolean regis(String regs){
     
         return false;
     }
}
