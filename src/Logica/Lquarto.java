/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Modelo.Mquarto;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lquarto {
    private Conexao mysql= new Conexao();
    private Connection cn=mysql.conectar();
    private String sSQL="";
    public Integer totalRegistro;
    
    
    public DefaultTableModel mostrar(String buscar) throws SQLException{
        DefaultTableModel modelo;
        String[] titulos ={"ID","NUMERO","DESCRIÇÃO","CARACTERISTICA","STATUS","TIPO"};
        String [] registro = new String[6];
        totalRegistro = 0;
        
        modelo = new DefaultTableModel(null, titulos);
        sSQL = "select * from quarto where numQuarto like '%" + buscar + "%' order by idQuarto";
        
        try{
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next()){
                registro[0]=rs.getString("idQuarto");
                registro[1]=rs.getString("numQuarto");
                registro[2]=rs.getString("descQuarto");
                registro[3]=rs.getString("caracQuarto");
                registro[4]=rs.getString("statusQuarto");
                registro[5]=rs.getString("tipoQuarto");
                
                
                totalRegistro = totalRegistro +1;
                modelo.addRow(registro);
            }
            return modelo;
        }catch(Exception ex){
            JOptionPane.showConfirmDialog(null, ex + " erro ao carregar a tabela de quarto");
            return null;
        }finally{
            cn.close();
        }
        
    }
    public DefaultTableModel buscaQuarto(String entra, String saida) throws SQLException{
        DefaultTableModel modelo;
        String[] titulos ={"ID","NUMERO","DESCRIÇÃO","CARACTERISTICA","STATUS","TIPO"};
        String [] registro = new String[6];
        totalRegistro = 0;
            
        
        modelo = new DefaultTableModel(null, titulos);
//        sSQL ="Select Distinct * From quarto q Where idQuarto not in "
//                + "(Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto)"
//                + " and (dataEntrada < '"+saida+"' and statusReser ='reservada' or 'confirmada' or 'hospedada')"
//                + " and (dataSaida > '"+entra+"' and statusReser ='reservada' or 'confirmada' or 'hospedada') "
//                + "and (DataSaida BETWEEN '"+entra+"' and '"+saida+"') or dataEntrada BETWEEN '"+entra+"' and '"+saida+"'"
//                + " or dataSaida > '"+entra+"' and statusreser='hospedada'"
//                + "or dataSaida<'"+saida+"' and dataEntrada <'"+entra+"'  and statusReser ='reservada' or 'confirmada' or 'hospedada') ";
//             sSQL="Select Distinct * From  quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto)  \n" +
//                    " and (dataEntrada < '"+ saida +"' and statusReser ='reservada' or 'confirmada' or 'hospedada')\n" +
//                    " or dataSaida > '"+entra+"' and statusreser='hospedada')";

                    ///consulta
//             sSQL="Select Distinct * From  quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto)and"
//                     + " (dataEntrada<='"+entra+"' and dataSaida>'"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')"
//                     + " and (dataEntrada<='"+ entra+"'or dataSaida<'"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')"
//                     + " or (dataEntrada and dataSaida BETWEEN '"+entra+"' and '"+saida+"')or (dataSaida > '"+entra+"' and statusreser='hospedada'))";
             //consulta
                sSQL="Select * From quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto)and"
                     + " (dataEntrada<='"+entra+"' and dataSaida>'"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')"
                     + " and (dataEntrada<='"+ entra+"'or dataSaida<'"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')"
                     + " or ( dataSaida BETWEEN '"+entra+"' + INTERVAL 1 day and '"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')or (dataSaida > '"+entra+"' and statusreser='hospedada')"
                        + " or(dataEntrada > '"+entra+"' - INTERVAL 1 day and dataEntrada<'"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada'))";
//                    "Select Distinct * From  quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto) " +
//                "and  (dataEntrada < '" + saida + "' and statusReser ='reservada' or 'confirmada' or 'hospedada')"+
//                    "and (dataEntrada between '" + entra + "' and '" + saida + "') and (statusReser ='reservada' or 'confirmada' or 'hospedada'))"
//        "Select Distinct * From  quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto) " +
//               "and dataEntrada between '" + entra + "' and '" + saida + "' " +
//               "and statusReser ='reservada' or dataSaida >'2019-01-07' and statusReser ='hospedada') or dataEntrada between '" + entra + "' and '" + saida + "' " +
//               "and statusReser ='confirmada')";

////                sSQL="Select Distinct * From  quarto q Where idQuarto not in (Select R.idQuarto From reserva R Join quarto q on (q.idQuarto = R.idQuarto) "
////                    + "and (dataEntrada<='"+entra+"' and dataSaida>='"+saida+"' and statusReser='reservada' or 'confirmada' or 'hospedada')" +
////                    "and (dataEntrada<='"+entra+"' or dataSaida<'"+entra+"' and statusReser='reservada' or 'confirmada' or 'hospedada') " +
////                    "or " +
////                    " (dataSaida >'"+saida+"' and statusreser='hospedada'))";        
        try{
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next()){
                registro[0]=rs.getString("idQuarto");
                registro[1]=rs.getString("numQuarto");
                registro[2]=rs.getString("descQuarto");
                registro[3]=rs.getString("caracQuarto");
                registro[4]=rs.getString("statusQuarto");
                registro[5]=rs.getString("tipoQuarto");
                
                
                totalRegistro = totalRegistro +1;
                modelo.addRow(registro);
            }
            return modelo;
        }catch(Exception ex){
            JOptionPane.showConfirmDialog(null,  ex + " erro ao carregar a tabela de quarto ===>" + entra.toString() );
            return null;
        }finally{
            cn.close();
        }
        
    }
    
    public boolean inserir (Mquarto dts) throws SQLException{
        sSQL = "insert into quarto (numQuarto, descQuarto, statusQuarto, tipoQuarto, caracQuarto)"+
                "values(?,?,?,?,?)";
    
    
            try {
                PreparedStatement pst = cn.prepareStatement(sSQL);
                pst.setString(1, dts.getNumQuarto());
                pst.setString(2, dts.getDescQuarto());
                pst.setString(3, dts.getStatusQuarto());
                pst.setString(4, dts.getTipoQuarto());
                pst.setString(5, dts.getCaracQuarto());
                
                int n=pst.executeUpdate();
                if (n!=0){
                return true;
                
            }else{
                    return false;
                    }
                    
            } catch (SQLException ex) {
                JOptionPane.showConfirmDialog(null, ex + "Erro ao inserir");
            }finally{
            cn.close();
        }
    
        return false;
    }
    public boolean editar(Mquarto dts) throws SQLException{
        sSQL="update quarto set numQuarto=?, descQuarto=?, statusQuarto=?, tipoQuarto=?, caracQuarto=?" +
                 "where idQuarto=?";
         try {
                PreparedStatement pst = cn.prepareStatement(sSQL);
                pst.setString(1, dts.getNumQuarto());
                pst.setString(2, dts.getDescQuarto());
                pst.setString(3, dts.getStatusQuarto());
                pst.setString(4, dts.getTipoQuarto());
                pst.setString(5, dts.getCaracQuarto());
                pst.setInt(6, dts.getIdQuarto());
                
                int n=pst.executeUpdate();
                if (n!=0){
                return true;
                
            }else{
                    return false;
                    }
                    
            } catch (SQLException ex) {
                JOptionPane.showConfirmDialog(null, ex + "Erro ao editar");
            }finally{
            cn.close();
        }
    
        return false;
    }
    public boolean ocupar (Mquarto dts1) throws SQLException{
        
        sSQL="update quarto set  statusQuarto='ocupado'" +
                "where idQuarto=? and statusQuarto='disponivel'"+"";
        
               
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
           
            pst.setInt(1, dts1.getIdQuarto());
            
             int n=pst.executeUpdate();
            if (n!=0){
                return true;
            }else{
                return false;
            }
            
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e+"\n"
                     + "o apartamento esta ocupado");
         return false;
        }finally{
            cn.close();
        }
    }
    public boolean desocupar (Mquarto dts1) throws SQLException{
        
        sSQL="update quarto set  statusQuarto='disponivel'" +
                "where numQuarto=?"+"";
        
               
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
           
            pst.setString(1, dts1.getNumQuarto());
            
             int n=pst.executeUpdate();
            if (n!=0){
                return true;
            }else{
                return false;
            }
            
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
         return false;
        }finally{
            cn.close();
        }
    }
    public boolean delete (Mquarto dts) throws SQLException{
        sSQL="delete from quarto where idQuarto=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdQuarto());
             int n=pst.executeUpdate();
            if (n!=0){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
             return false;
        }finally{
            cn.close();
        }
    }
    
}
