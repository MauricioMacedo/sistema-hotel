/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Modelo.Mreserva;
import java.awt.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lreserva {
    private Conexao mysql=new Conexao();
    private Connection cn=mysql.conectar();
    private String sSQL="";
    public Integer totalregistro;
    public Integer cafes;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    
    
    public DefaultTableModel mostrar (String buscar) throws SQLException{
       DefaultTableModel modelo;
       String[] titulo={"COD","APT","HOSPEDE","FUNCIONARIO","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
       String[] registro=new String[10];
       totalregistro=0;
       
       modelo= new DefaultTableModel(null, titulo);
       sSQL="SELECT idReser,r.idQuarto, r.idHosp,r.idFunc, r.qtdeHosp,r.dataReser,r.dataEntrada,r.dataSaida, " +
             "r.valorReserva,r.statusReser,r.obsReser, r.tipoReser, " +
             "q.numQuarto, h.nomeHosp , f.funcionario FROM reserva r " +
             "INNER JOIN quarto q ON q.idQuarto= r.idQuarto " +
             "INNER JOIN hospede h ON h.idHosp = r.idHosp " +
             "INNER JOIN funcionario f ON f.idFunc=r.idFunc " +
             "WHERE r.statusReser like '%"+ buscar +"%' and r.statusReser!='fechada' order by r.dataEntrada asc ";
               
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
                registro[0]=rs.getString("idReser");
                registro[1]=rs.getString("numQuarto");
                registro[2]=rs.getString("nomeHosp");
                registro[3]=rs.getString("funcionario");
                registro[4]=sdf.format(rs.getDate("dataEntrada"));
                registro[5]=sdf.format(rs.getDate("dataSaida"));
                registro[6]=rs.getString("valorReserva");
                registro[7]=rs.getString("statusReser");
                registro[8]=rs.getString("obsReser");
                registro[9]=rs.getString("tipoReser");
                
                totalregistro=totalregistro+1;
                modelo.addRow(registro);
            }
            return modelo;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de entrada");
             return null;
        }finally{
            cn.close();
        }
    } 
    
        public DefaultTableModel ok (String buscar) throws SQLException{
       DefaultTableModel ok;     
       String[] titulo={"TIPO"};
       String[] registro=new String[1];
       totalregistro=0;       
       ok = new DefaultTableModel(null,titulo);
       sSQL="select idReser from reserva where dataEntrada='"+buscar+"' and statusReser='reservada' order by dataEntrada asc";
            
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
               registro[0]=rs.getString("idReser");
                totalregistro=totalregistro+1;
               ok.addRow(registro);
            }
            return ok;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de verificação de check-out");
             return null;
        }finally{
            cn.close();
        }
    } 
       public DefaultTableModel verSaida (String buscar){
       DefaultTableModel ok;     
       String[] titulo={"TIPO"};
       String[] registro=new String[1];
       totalregistro=0;       
       ok = new DefaultTableModel(null,titulo);
       sSQL="select idReser from reserva where dataSaida='"+buscar+"' and statusReser='hospedada' order by dataEntrada asc";
            
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
               registro[0]=rs.getString("idReser");
                totalregistro=totalregistro+1;
               ok.addRow(registro);
            }
            return ok;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de verificação de check-out");
             return null;
        }
    } 
    public DefaultTableModel checkin (String buscar){
       DefaultTableModel modelo;
       String[] titulo={"COD","ID APT","ID HOSP","ID FUNC","APT","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
       String[] registro=new String[15];
       totalregistro=0;
       
       modelo= new DefaultTableModel(null, titulo);
       sSQL="SELECT r.idReser,r.idQuarto, r.idHosp,r.idFunc, r.qtdeHosp,r.dataReser,r.dataEntrada,r.dataSaida, " +
             "r.valorReserva,r.statusReser,r.obsReser, r.tipoReser, " +
             "q.numQuarto, h.nomeHosp , f.funcionario FROM reserva r " +
             "INNER JOIN quarto q ON q.idQuarto= r.idQuarto " +
             "INNER JOIN hospede h ON h.idHosp = r.idHosp " +
             "INNER JOIN funcionario f ON f.idFunc=r.idFunc " +
             "WHERE r.statusReser='reservada' and r.autoDiaria!='confirmada' and h.nomeHosp like '%"+ buscar +"%' "
               + "or (r.dataEntrada like '%" + buscar + "%'and r.statusReser='reservada' and r.autoDiaria!='confirmada') order by r.dataEntrada asc";
               
       
       
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
                registro[0]=rs.getString("idReser");
                registro[1]=rs.getString("idQuarto");
                registro[2]=rs.getString("idHosp");
                registro[3]=rs.getString("idFunc");
                registro[4]=rs.getString("numQuarto");
                registro[5]=rs.getString("nomeHosp");
                registro[6]=rs.getString("funcionario");
                registro[7]=rs.getString("qtdeHosp");
                registro[8]=rs.getString("dataReser");
                registro[9]=rs.getString("dataEntrada");
                registro[10]=rs.getString("dataSaida");
                registro[11]=rs.getString("valorReserva");
                registro[12]=rs.getString("statusReser");
                registro[13]=rs.getString("obsReser");
                registro[14]=rs.getString("tipoReser");
                
                totalregistro=totalregistro+1;
                modelo.addRow(registro);
                
            }
            return modelo;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de entrada");
             return null;
        }
    }
    
     public DefaultTableModel confirmada (String buscar){
       DefaultTableModel modelo;
       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","APT","HOSPEDE","FUNCIONARIO","QTDE","RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
       String[] registro=new String[15];
       totalregistro=0;
       
       modelo= new DefaultTableModel(null, titulo);
       sSQL="SELECT idReser,r.idQuarto, r.idHosp,r.idFunc, r.qtdeHosp,r.dataReser,r.dataEntrada,r.dataSaida,"
               + "r.valorReserva,r.statusReser,r.obsReser, r.tipoReser, "
               + "q.numQuarto, h.nomeHosp , f.funcionario FROM reserva r "
               + "INNER JOIN quarto q ON q.idQuarto= r.idQuarto "
               + "INNER JOIN hospede h ON h.idHosp = r.idHosp "
               + "INNER JOIN funcionario f ON f.idFunc=r.idFunc "
               + "WHERE r.statusReser='reservada' and r.autoDiaria='confirmada' and h.nomeHosp like '%"+ buscar +"%'";
       
//       
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
                registro[0]=rs.getString("r.idReser");
                registro[1]=rs.getString("idQuarto");
                registro[2]=rs.getString("idHosp");
                registro[3]=rs.getString("idFunc");
                registro[4]=rs.getString("numQuarto");
                registro[5]=rs.getString("nomeHosp");
                registro[6]=rs.getString("funcionario");
                registro[7]=rs.getString("qtdeHosp");
                registro[8]=rs.getString("dataReser");
                registro[9]=rs.getString("dataEntrada");
                registro[10]=rs.getString("dataSaida");
                registro[11]=rs.getString("valorReserva");
                registro[12]=rs.getString("statusReser");
                registro[13]=rs.getString("obsReser");
                registro[14]=rs.getString("tipoReser");
                
                totalregistro=totalregistro+1;
                modelo.addRow(registro);
            }
            return modelo;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de confirmada, Logica");
         return null;
        }
    }
     
     
      public DefaultTableModel hospedadas (String buscar){
       DefaultTableModel modelo;
       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","APT","HOSPEDE","FUNCIONARIO","QTDE",
           "RESERVA","ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
       String[] registro=new String[15];
       totalregistro=0;
       cafes=0;
       
       modelo= new DefaultTableModel(null, titulo);
       sSQL="SELECT idReser,r.idQuarto, r.idHosp,r.idFunc, r.qtdeHosp,r.dataReser,r.dataEntrada,r.dataSaida,"
               + "r.valorReserva,r.statusReser,r.obsReser, r.tipoReser, "
               + "q.numQuarto, h.nomeHosp , f.funcionario FROM reserva r "
               + "INNER JOIN quarto q ON q.idQuarto= r.idQuarto "
               + "INNER JOIN hospede h ON h.idHosp = r.idHosp "
               + "INNER JOIN funcionario f ON f.idFunc=r.idFunc "
               + "WHERE r.statusReser='hospedada' and h.nomeHosp like '%"+ buscar +"%' or r.dataEntrada like '%" + buscar + "%' and r.statusReser='hospedada' order by r.dataSaida asc";
               
       
       
        try {
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next())
            {
                registro[0]=rs.getString("r.idReser");
                registro[1]=rs.getString("idQuarto");
                registro[2]=rs.getString("idHosp");
                registro[3]=rs.getString("idFunc");
                registro[4]=rs.getString("numQuarto");
                registro[5]=rs.getString("nomeHosp");
                registro[6]=rs.getString("funcionario");
                registro[7]=rs.getString("qtdeHosp");
                registro[8]=rs.getString("dataReser");
                registro[9]=rs.getString("dataEntrada");
                registro[10]=rs.getString("dataSaida");
                registro[11]=rs.getString("valorReserva");
                registro[12]=rs.getString("statusReser");
                registro[13]=rs.getString("obsReser");
                registro[14]=rs.getString("tipoReser");
                
                totalregistro=totalregistro+1;
                cafes = cafes + (rs.getInt("qtdeHosp"));
                modelo.addRow(registro);
            }
            return modelo;
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de hospedadas");
             return null;
        }
        
    }
       public DefaultTableModel fechada (String buscar){
       DefaultTableModel modelo;
       String[] titulo={"COD","ID QUARTO","ID HOSP","ID FUNC","QUARTO","HOSPEDE","FUNCIONARIO","QTDE","RESERVA",
           "ENTRADA","SAIDA","VALOR","STATUS","OBS","TIPO"};
       String[] registro=new String[15];
       totalregistro=0;
       
//       modelo= new DefaultTableModel(null, titulo);
//       sSQL="select r.idReser,q.numQuarto,h.nomeHosp,f.funcionario,r.dataEntrada,r.dataSaida,r.valorReserva,"
//               + "r.statusReser,r.obsReser,r.tipoReser from reserva r, quarto q, hospede h, funcionario f where "
//               + "r.idQuarto=q.idQuarto and r.idHosp=h.idHosp "
//               + "and r.idFunc=f.idFunc and r.idReser='"+ buscar +"' and r.statusReser='fechada' order by r.dataEntrada desc";
//       
//       
//        try {
//            Statement st = cn.createStatement();
//            ResultSet rs=st.executeQuery(sSQL);
//            while(rs.next())
//            {
//                registro[0]=rs.getString("idReser");
//                registro[1]=rs.getString("idQuarto");
//                registro[2]=rs.getString("idHosp");
//                registro[3]=rs.getString("idFunc");
//                registro[4]=rs.getString("numQuarto");
//                registro[5]=rs.getString("nomeHosp");
//                registro[6]=rs.getString("funcionario");
//                registro[7]=rs.getString("qtdeHosp");
//                registro[8]=rs.getString("dataReser");
//                registro[9]=rs.getString("dataEntrada");
//                registro[10]=rs.getString("dataSaida");
//                registro[11]=rs.getString("valorReserva");
//                registro[12]=rs.getString("statusReser");
//                registro[13]=rs.getString("obsReser");
//                registro[14]=rs.getString("tipoReser");
//                
//                totalregistro=totalregistro+1;
//                modelo.addRow(registro);
//            }
//            return modelo;
//        } catch (SQLException ex) {
//            JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de vencida");
             return null;
//        }
    }
    public boolean inserir(Mreserva dts)
    {
       sSQL="insert into reserva (idQuarto,idHosp,idFunc,qtdeHosp,dataReser,dataEntrada,dataSaida,valorReserva,statusReser,obsReser,tipoReser)"+
               "values(?,?,?,?,now(),?,?,?,?,?,?)"; 
       
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdQuarto());
            pst.setInt(2, dts.getIdHosp());
            pst.setInt(3, dts.getIdFunc());
            pst.setInt(4, dts.getQtdeHosp());
            pst.setDate(5, dts.getDataEntrada());
            pst.setDate(6, dts.getDataSaida());
            pst.setDouble(7, dts.getValorReserva());
            pst.setString(8, dts.getStatusReser());
            pst.setString(9, dts.getObsReser());
            pst.setString(10, dts.getTipoReser());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao inserir");
            return false;
        }
       
    }
    public boolean editar(Mreserva dts){
        sSQL="update reserva set idQuarto=?, idHosp=?, idFunc=?,qtdeHosp=?,dataReser=?,dataEntrada=?,dataSaida=?,valorReserva=?, statusReser=?,obsReser=?,tipoReser=? "+
             " where idReser=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdQuarto());
            pst.setInt(2, dts.getIdHosp());
            pst.setInt(3, dts.getIdFunc());
            pst.setInt(4, dts.getQtdeHosp());
            pst.setDate(5,dts.getDataReser());
            pst.setDate(6, dts.getDataEntrada());
            pst.setDate(7, dts.getDataSaida());
            pst.setDouble(8, dts.getValorReserva());
            pst.setString(9, dts.getStatusReser());
            pst.setString(10, dts.getObsReser());
            pst.setString(11, dts.getTipoReser());
            pst.setInt(12,dts.getIdReser());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao tentar atualizar");
            return false;        }
        
    }
    public boolean delete(Mreserva dts){
         sSQL="delete from reserva where idReser=?";
        
       
        try {
            
             PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser());
        int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao deletar");
            return false; 
       
    }
}
    public boolean mudaSituacao(Mreserva dts){
        sSQL="update reserva set statusReser='hospedada' where idReser=?";
        
       
        try {
            
             PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser());
        int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao mudar situação reserva");
            return false; 
       
    }
}
     public boolean mudaConfirma(Mreserva dts){
        sSQL="update reserva set autoDiaria='confirmada' where idReser=?";
        
       
        try {
            
             PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser());
        int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao mudar situação reserva");
            return false; 
       
    }
}
     void fechar(){
        try {
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Lreserva.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    
}
