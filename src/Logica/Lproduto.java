/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Modelo.Mproduto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lproduto {
    private Conexao mysql=new Conexao();
    private Connection cn=mysql.conectar();
    private String sSQL="";
    public Integer totalregistro;
    
    public DefaultTableModel mostrar (String buscar) throws SQLException{
        
            DefaultTableModel modelo;
            String[] titulos= {"ID","PRODUTO","VALOR R$:","DESCRIÇÃO","UNIDADE"};
            String[] registro= new String[5];
            totalregistro=0;
            
            modelo = new DefaultTableModel(null,titulos);
            sSQL="select * from produto where nomeProd like '%" + buscar + "%' and idProd>2 order by idProd";
        try{    
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sSQL);
            while(rs.next()){
                registro[0]=rs.getString("idProd");
                registro[1]=rs.getString("nomeProd");
                registro[2]=rs.getString("valorProd");
                registro[3]=rs.getString("descProd");
                registro[4]=rs.getString("unidProduto");
                
                totalregistro=totalregistro+1;
                modelo.addRow(registro);
                
            }
            return modelo;
        } catch (SQLException ex) {
             JOptionPane.showConfirmDialog(null, ex + " Erro ao carregar a tabela de funcionário");
           return null;
        }finally{
            cn.close();
        }
     }
    public boolean inserir(Mproduto dts) throws SQLException{
        sSQL="insert into produto (nomeProd, valorProd, descProd, unidProduto)"+
                "values(?,?,?,?)";
        
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setString(1, dts.getNomeProd());
            pst.setDouble(2, dts.getValorProd());
            pst.setString(3, dts.getDescProd());
            pst.setString(4, dts.getUnidProduto());
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex + " Erro ao inserir");
            return false;
        }finally{
            cn.close();
        }
    }
    public boolean editar(Mproduto dts) throws SQLException{
        sSQL="update produto set nomeProd=?, valorProd=?, descProd=?, unidProduto=?"+
                "where idProd=?";
       
            try {
                PreparedStatement pst=cn.prepareStatement(sSQL);
                pst.setString(1, dts.getNomeProd());
                pst.setDouble(2, dts.getValorProd());
                pst.setString(3, dts.getDescProd());
                pst.setString(4, dts.getUnidProduto());
                pst.setInt(5, dts.getIdProd());
                
                int n=pst.executeUpdate();
                if(n!=0){
                    return true;
                }else{
                    return false;
                }
                   
            } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, ex + " Erro ao editar");
            return false;
            }finally{
            cn.close();
        }
           
       } 
    public boolean delete(Mproduto dts) throws SQLException{
        sSQL="delete from produto where idProd=?";
        
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdProd());
            
            int n =pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex + " Erro ao apagar o registro ");
            return false;        
        }finally{
            cn.close();
        }
    }
    
}
