/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;




import Modelo.Mentrada;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lentrada {
    
 private Conexao mysql= new Conexao();
 private Connection cn=mysql.conectar();
 private String sSQL=""; 
 public Integer totalRegistro; 
 
 public DefaultTableModel mostrar(String buscar) throws SQLException{ //para trabalhar as busca.
        
        DefaultTableModel modelo;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
     
       
        String[] titulos = {"CODIGO","COD PROD","PRODUTO","QUANTIDADE","VALOR","DATA ENTRADA","FUNCIONARIO"};
        String[] registro = new String[7];  
        totalRegistro = 0;
        
        modelo = new DefaultTableModel(null, titulos);//concatenar os valores do bd
        sSQL = "select e.idEntra, e.idProd, e.qtdeProd, e.valorUnit, e.dataEntra, e.funcionario, p.nomeProd FROM entrada e,"
                + " produto p where e.idProd=p.idProd and p.nomeProd like '%"+ buscar +"%' or e.dataEntra like '%"+ buscar +"%' order by idEntra desc";
        
        
        //passar os campos de cadastro para o banco de dados bd
        try{ 
            Statement st = cn.createStatement();
            ResultSet  rs=st.executeQuery(sSQL); 
            while(rs.next()){
                registro [0]=rs.getString("idEntra");
                registro [1]=rs.getString("idProd");
                registro [2]=rs.getString("nomeProd");
                registro [3]=rs.getString("qtdeProd");
                registro [4]=rs.getString("valorUnit");
                registro [5]=sdf.format(rs.getDate("dataEntra"));
                registro [6]=rs.getString("funcionario");
                                             
                //laço para sempre que adicionar um quarto gerar mais um registro
                totalRegistro = totalRegistro +1;
                modelo.addRow(registro);
            }
            return modelo; // retornar os resultados
        }catch (Exception e){  // retorna o erro e a informação 
            JOptionPane.showConfirmDialog(null, e);
            return null;            
        }finally{
            cn.close();
        }
                
    }
 public boolean inserir (Mentrada dts) throws SQLException{
        sSQL = "insert into entrada(idProd, qtdeProd, valorUnit,dataEntra,funcionario)" +
                "values(?,?,?,now(),?)";
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdProd());             
                   
            pst.setInt(2, dts.getQtdeProd());
            pst.setDouble(3, dts.getValorUnit());
            pst.setString(4, dts.getFuncionario()); 
                                     
                       
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e);
            return false;
                    
        }finally{
            cn.close();
        }
    } 
  public boolean editar (Mentrada dts) throws SQLException{
        sSQL = "update entrada set idProd=?,qtdeProd=?,valorUnit=?,dataEntra=?, funcionario=?" +
                "where idEntra=?";
        try{
             PreparedStatement pst=cn.prepareStatement(sSQL);
              
            pst.setInt(1, dts.getIdProd());             
                    
            pst.setInt(2, dts.getQtdeProd());
            pst.setDouble(3, dts.getValorUnit());
            pst.setDate(4,(Date)dts.getDataEntra());
            pst.setString(5, dts.getFuncionario()); 
            pst.setInt(6, dts.getIdEntra());
                               
            
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e);
            return false;
                    
        }finally{
            cn.close();
        }
    } 
  public boolean delete (Mentrada dts) throws SQLException{
        sSQL="delete from entrada where idEntra=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdEntra());
             int n=pst.executeUpdate();
            if (n!=0){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
             return false;
        }finally{
            cn.close();
        }
    }
}
