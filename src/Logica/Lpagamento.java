/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;




import Modelo.Mpagamento;
import Modelo.Mreserva;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Macedo
 */
public class Lpagamento {
    
 private Conexao mysql= new Conexao();
 private Connection cn=mysql.conectar();
 private String sSQL=""; 
 public Integer totalRegistro; 
  public Double totalPagar; 
 
 public DefaultTableModel mostrar(String buscar) throws SQLException{ //para trabalhar as busca.
        
        DefaultTableModel modelo;
       
        String[] titulos = {"CODIGO","COD RESER","COD FUNC","HOSPEDE","FUNCIONARIO","APT","VALOR","TIPO","DATA","COD HOSP"};
        String[] registro = new String[10];  
        totalRegistro = 0;
        totalPagar =0.0;
        
        modelo = new DefaultTableModel(null, titulos);//concatenar os valores do bd
        sSQL = "select p.idPag, p.idReser,p.idFunc, p.valorTotal, p.tipoPag,p.dataPag,r.idHosp, r.idQuarto, "
                + "h.nomeHosp,f.funcionario, q.numQuarto FROM pagamento p inner join reserva r on p.idReser=r.idReser "
                + "inner join hospede h on r.idHosp=h.idHosp inner join funcionario f on p.idFunc=f.idFunc inner join "
                + "quarto q on r.idQuarto=q.idQuarto where p.idreser='"+ buscar +"'  "
                + "order by idReser desc";
        
        
        //passar os campos de cadastro para o banco de dados bd
        try{ 
            Statement st = cn.createStatement();
            ResultSet  rs=st.executeQuery(sSQL); 
            while(rs.next()){
                registro [0]=rs.getString("idPag");
                registro [1]=rs.getString("idReser");
                registro [2]=rs.getString("idFunc");
                registro [3]=rs.getString("nomeHosp");
                registro [4]=rs.getString("funcionario");
                registro [5]=rs.getString("numQuarto");
                registro [6]=rs.getString("valorTotal");
                registro [7]=rs.getString("tipoPag");
                registro [8]=rs.getString("dataPag");
                registro [9]=rs.getString("idHosp");
                
                                             
                //laço para sempre que adicionar um quarto gerar mais um registro
                totalRegistro = totalRegistro +1;
                totalPagar = totalPagar + rs.getDouble("valorTotal");
                modelo.addRow(registro);
            }
            return modelo; // retornar os resultados
        }catch (Exception e){  // retorna o erro e a informação 
            JOptionPane.showConfirmDialog(null, e);
            return null;            
        }finally{
            cn.close();
        }
                
    }
 public boolean inserir (Mpagamento dts) throws SQLException{
        sSQL = "insert into pagamento(idReser, idFunc,valorTotal,tipoPag,dataPag)" +
                "values(?,?,?,?,now())";
        try{
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser()); 
            pst.setInt(2, dts.getIdfunc()); 
            
            pst.setDouble(3, dts.getValortotal());
            pst.setString(4, dts.getTipopag()); 
                    
                                     
                       
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e + "erro ao inserir");
            return false;
                    
        }finally{
            cn.close();
        }
    } 
  public boolean editar (Mpagamento dts) throws SQLException{
        sSQL = "update pagamento set idReser=?, idFunc=?,valorTotal=?,tipoPag=?,dataPag=? where idPag=? ";
        try{
             PreparedStatement pst=cn.prepareStatement(sSQL);
              
             pst.setInt(1, dts.getIdReser()); 
            pst.setInt(2, dts.getIdfunc()); 
            
            pst.setDouble(3, dts.getValortotal());
            pst.setString(4, dts.getTipopag());  
            pst.setDate(5, dts.getDatapag());
            pst.setInt(6, dts.getIdpag());                               
            
            
            int n=pst.executeUpdate();
            if(n!=0){
                return true;                
            }
            else {
                return false;
            }
        }catch (Exception e){// se de algum erro ele retornara coomo falso
            JOptionPane.showConfirmDialog(null, e + " erro ao editar");
            return false;
                    
        }finally{
            cn.close();
        }
    } 
  public boolean delete (Mpagamento dts) throws SQLException{
        sSQL="delete from pagamento where idPag=?";
        
        try {
            PreparedStatement pst=cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdpag());
             int n=pst.executeUpdate();
            if (n!=0){
                return true; 
            }else{
                return false;
            }
        }catch (Exception e){
             JOptionPane.showConfirmDialog(null, e);
             return false;
        }finally{
            cn.close();
        }
    }
  
  public boolean mudaSituacaoPag(Mreserva dts) throws SQLException{
        sSQL="update reserva set statusReser='fechada' where idReser=?";
        
       
        try {
            
             PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdReser());
        int n=pst.executeUpdate();
            if(n!=0){
                return true;
            }else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex+ "Erro ao fechar a reserva\n +==>mudar situação");
            return false; 
       
    }finally{
            cn.close();
        }
}

}
